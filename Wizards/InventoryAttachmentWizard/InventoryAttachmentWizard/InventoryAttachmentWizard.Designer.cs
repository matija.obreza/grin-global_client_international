﻿namespace InventoryAttachmentWizard
{
    partial class InventoryAttachmentWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryAttachmentWizard));
            this.ux_datagridviewAttachments = new System.Windows.Forms.DataGridView();
            this.ux_buttonBrowse = new System.Windows.Forms.Button();
            this.ux_splitcontainerMain = new System.Windows.Forms.SplitContainer();
            this.ux_checkboxViewExistingAttachments = new System.Windows.Forms.CheckBox();
            this.ux_treeviewInventoryList = new System.Windows.Forms.TreeView();
            this.ux_splitcontainerAttachmentDataDisplay = new System.Windows.Forms.SplitContainer();
            this.ux_listviewAttachments = new System.Windows.Forms.ListView();
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            this.ux_tabpageFormView = new System.Windows.Forms.TabPage();
            this.ux_panelFormView = new System.Windows.Forms.Panel();
            this.ux_labelDescriptionCode = new System.Windows.Forms.Label();
            this.ux_comboboxDescriptionCode = new System.Windows.Forms.ComboBox();
            this.ux_textboxDescription = new System.Windows.Forms.TextBox();
            this.ux_textboxCopyrightInformation = new System.Windows.Forms.TextBox();
            this.ux_labelDescription = new System.Windows.Forms.Label();
            this.ux_labelInventoryNumber = new System.Windows.Forms.Label();
            this.ux_textboxTitle = new System.Windows.Forms.TextBox();
            this.ux_textboxInventoryNumber = new System.Windows.Forms.TextBox();
            this.ux_labelTitle = new System.Windows.Forms.Label();
            this.ux_labelSortOrder = new System.Windows.Forms.Label();
            this.ux_labelAttachCooperator = new System.Windows.Forms.Label();
            this.ux_textboxSortOrder = new System.Windows.Forms.TextBox();
            this.ux_textboxAttachCooperator = new System.Windows.Forms.TextBox();
            this.ux_labelNote = new System.Windows.Forms.Label();
            this.ux_comboboxAttachDateCode = new System.Windows.Forms.ComboBox();
            this.ux_textboxNote = new System.Windows.Forms.TextBox();
            this.ux_labelAttachDateCode = new System.Windows.Forms.Label();
            this.ux_labelCopyrightInformation = new System.Windows.Forms.Label();
            this.ux_textboxAttachDate = new System.Windows.Forms.TextBox();
            this.ux_checkboxIsWebVisible = new System.Windows.Forms.CheckBox();
            this.ux_labelAttachDate = new System.Windows.Forms.Label();
            this.ux_labelCategoryCode = new System.Windows.Forms.Label();
            this.ux_textboxContentType = new System.Windows.Forms.TextBox();
            this.ux_comboboxCategoryCode = new System.Windows.Forms.ComboBox();
            this.ux_labelContentType = new System.Windows.Forms.Label();
            this.ux_tabpageGridView = new System.Windows.Forms.TabPage();
            this.ux_openfiledialog = new System.Windows.Forms.OpenFileDialog();
            this.ux_progressbar = new System.Windows.Forms.ProgressBar();
            this.ux_labelProgressValue = new System.Windows.Forms.Label();
            this.ux_labelProgressDescription = new System.Windows.Forms.Label();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonSaveAndExit = new System.Windows.Forms.Button();
            this.ux_menustripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerMain)).BeginInit();
            this.ux_splitcontainerMain.Panel1.SuspendLayout();
            this.ux_splitcontainerMain.Panel2.SuspendLayout();
            this.ux_splitcontainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerAttachmentDataDisplay)).BeginInit();
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.SuspendLayout();
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.SuspendLayout();
            this.ux_splitcontainerAttachmentDataDisplay.SuspendLayout();
            this.ux_tabcontrolMain.SuspendLayout();
            this.ux_tabpageFormView.SuspendLayout();
            this.ux_panelFormView.SuspendLayout();
            this.ux_tabpageGridView.SuspendLayout();
            this.ux_menustripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_datagridviewAttachments
            // 
            this.ux_datagridviewAttachments.AllowUserToAddRows = false;
            this.ux_datagridviewAttachments.AllowUserToOrderColumns = true;
            this.ux_datagridviewAttachments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewAttachments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewAttachments.Location = new System.Drawing.Point(3, 3);
            this.ux_datagridviewAttachments.Name = "ux_datagridviewAttachments";
            this.ux_datagridviewAttachments.Size = new System.Drawing.Size(683, 256);
            this.ux_datagridviewAttachments.TabIndex = 0;
            // 
            // ux_buttonBrowse
            // 
            this.ux_buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonBrowse.Location = new System.Drawing.Point(191, 3);
            this.ux_buttonBrowse.Name = "ux_buttonBrowse";
            this.ux_buttonBrowse.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonBrowse.TabIndex = 1;
            this.ux_buttonBrowse.Text = "Browse...";
            this.ux_buttonBrowse.UseVisualStyleBackColor = true;
            this.ux_buttonBrowse.Click += new System.EventHandler(this.ux_buttonBrowse_Click);
            // 
            // ux_splitcontainerMain
            // 
            this.ux_splitcontainerMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerMain.Location = new System.Drawing.Point(4, 56);
            this.ux_splitcontainerMain.Name = "ux_splitcontainerMain";
            // 
            // ux_splitcontainerMain.Panel1
            // 
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_checkboxViewExistingAttachments);
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_treeviewInventoryList);
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_buttonBrowse);
            // 
            // ux_splitcontainerMain.Panel2
            // 
            this.ux_splitcontainerMain.Panel2.Controls.Add(this.ux_splitcontainerAttachmentDataDisplay);
            this.ux_splitcontainerMain.Size = new System.Drawing.Size(977, 589);
            this.ux_splitcontainerMain.SplitterDistance = 273;
            this.ux_splitcontainerMain.TabIndex = 2;
            // 
            // ux_checkboxViewExistingAttachments
            // 
            this.ux_checkboxViewExistingAttachments.AutoSize = true;
            this.ux_checkboxViewExistingAttachments.Location = new System.Drawing.Point(6, 9);
            this.ux_checkboxViewExistingAttachments.Name = "ux_checkboxViewExistingAttachments";
            this.ux_checkboxViewExistingAttachments.Size = new System.Drawing.Size(150, 17);
            this.ux_checkboxViewExistingAttachments.TabIndex = 2;
            this.ux_checkboxViewExistingAttachments.Text = "View Existing Attachments";
            this.ux_checkboxViewExistingAttachments.UseVisualStyleBackColor = true;
            this.ux_checkboxViewExistingAttachments.CheckedChanged += new System.EventHandler(this.ux_checkboxViewExistingAttachments_CheckedChanged);
            // 
            // ux_treeviewInventoryList
            // 
            this.ux_treeviewInventoryList.AllowDrop = true;
            this.ux_treeviewInventoryList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_treeviewInventoryList.Location = new System.Drawing.Point(3, 32);
            this.ux_treeviewInventoryList.Name = "ux_treeviewInventoryList";
            this.ux_treeviewInventoryList.Size = new System.Drawing.Size(263, 550);
            this.ux_treeviewInventoryList.TabIndex = 0;
            this.ux_treeviewInventoryList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ux_treeviewInventoryList_AfterSelect);
            this.ux_treeviewInventoryList.DragDrop += new System.Windows.Forms.DragEventHandler(this.ux_treeviewInventoryList_DragDrop);
            this.ux_treeviewInventoryList.DragEnter += new System.Windows.Forms.DragEventHandler(this.ux_treeviewInventoryList_DragEnter);
            // 
            // ux_splitcontainerAttachmentDataDisplay
            // 
            this.ux_splitcontainerAttachmentDataDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerAttachmentDataDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_splitcontainerAttachmentDataDisplay.Location = new System.Drawing.Point(0, 0);
            this.ux_splitcontainerAttachmentDataDisplay.Name = "ux_splitcontainerAttachmentDataDisplay";
            this.ux_splitcontainerAttachmentDataDisplay.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ux_splitcontainerAttachmentDataDisplay.Panel1
            // 
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.Controls.Add(this.ux_listviewAttachments);
            // 
            // ux_splitcontainerAttachmentDataDisplay.Panel2
            // 
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.Controls.Add(this.ux_tabcontrolMain);
            this.ux_splitcontainerAttachmentDataDisplay.Size = new System.Drawing.Size(700, 589);
            this.ux_splitcontainerAttachmentDataDisplay.SplitterDistance = 270;
            this.ux_splitcontainerAttachmentDataDisplay.TabIndex = 0;
            // 
            // ux_listviewAttachments
            // 
            this.ux_listviewAttachments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listviewAttachments.Location = new System.Drawing.Point(3, 3);
            this.ux_listviewAttachments.Name = "ux_listviewAttachments";
            this.ux_listviewAttachments.Size = new System.Drawing.Size(689, 260);
            this.ux_listviewAttachments.TabIndex = 0;
            this.ux_listviewAttachments.UseCompatibleStateImageBehavior = false;
            this.ux_listviewAttachments.ItemActivate += new System.EventHandler(this.ux_listviewAttachments_ItemActivate);
            this.ux_listviewAttachments.SelectedIndexChanged += new System.EventHandler(this.ux_listviewAttachments_SelectedIndexChanged);
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageFormView);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageGridView);
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(3, 3);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(693, 307);
            this.ux_tabcontrolMain.TabIndex = 0;
            // 
            // ux_tabpageFormView
            // 
            this.ux_tabpageFormView.Controls.Add(this.ux_panelFormView);
            this.ux_tabpageFormView.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageFormView.Name = "ux_tabpageFormView";
            this.ux_tabpageFormView.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageFormView.Size = new System.Drawing.Size(685, 281);
            this.ux_tabpageFormView.TabIndex = 0;
            this.ux_tabpageFormView.Text = "Form View";
            this.ux_tabpageFormView.UseVisualStyleBackColor = true;
            // 
            // ux_panelFormView
            // 
            this.ux_panelFormView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_panelFormView.Controls.Add(this.ux_labelDescriptionCode);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxDescriptionCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxDescription);
            this.ux_panelFormView.Controls.Add(this.ux_textboxCopyrightInformation);
            this.ux_panelFormView.Controls.Add(this.ux_labelDescription);
            this.ux_panelFormView.Controls.Add(this.ux_labelInventoryNumber);
            this.ux_panelFormView.Controls.Add(this.ux_textboxTitle);
            this.ux_panelFormView.Controls.Add(this.ux_textboxInventoryNumber);
            this.ux_panelFormView.Controls.Add(this.ux_labelTitle);
            this.ux_panelFormView.Controls.Add(this.ux_labelSortOrder);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachCooperator);
            this.ux_panelFormView.Controls.Add(this.ux_textboxSortOrder);
            this.ux_panelFormView.Controls.Add(this.ux_textboxAttachCooperator);
            this.ux_panelFormView.Controls.Add(this.ux_labelNote);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxAttachDateCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxNote);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachDateCode);
            this.ux_panelFormView.Controls.Add(this.ux_labelCopyrightInformation);
            this.ux_panelFormView.Controls.Add(this.ux_textboxAttachDate);
            this.ux_panelFormView.Controls.Add(this.ux_checkboxIsWebVisible);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachDate);
            this.ux_panelFormView.Controls.Add(this.ux_labelCategoryCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxContentType);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxCategoryCode);
            this.ux_panelFormView.Controls.Add(this.ux_labelContentType);
            this.ux_panelFormView.Location = new System.Drawing.Point(0, 0);
            this.ux_panelFormView.Name = "ux_panelFormView";
            this.ux_panelFormView.Size = new System.Drawing.Size(706, 292);
            this.ux_panelFormView.TabIndex = 1;
            // 
            // ux_labelDescriptionCode
            // 
            this.ux_labelDescriptionCode.AutoSize = true;
            this.ux_labelDescriptionCode.Location = new System.Drawing.Point(6, 129);
            this.ux_labelDescriptionCode.Name = "ux_labelDescriptionCode";
            this.ux_labelDescriptionCode.Size = new System.Drawing.Size(88, 13);
            this.ux_labelDescriptionCode.TabIndex = 115;
            this.ux_labelDescriptionCode.Tag = "description_code";
            this.ux_labelDescriptionCode.Text = "Description Code";
            // 
            // ux_comboboxDescriptionCode
            // 
            this.ux_comboboxDescriptionCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxDescriptionCode.FormattingEnabled = true;
            this.ux_comboboxDescriptionCode.Location = new System.Drawing.Point(9, 144);
            this.ux_comboboxDescriptionCode.Name = "ux_comboboxDescriptionCode";
            this.ux_comboboxDescriptionCode.Size = new System.Drawing.Size(158, 21);
            this.ux_comboboxDescriptionCode.TabIndex = 114;
            this.ux_comboboxDescriptionCode.Tag = "description_code";
            // 
            // ux_textboxDescription
            // 
            this.ux_textboxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxDescription.Location = new System.Drawing.Point(443, 22);
            this.ux_textboxDescription.Multiline = true;
            this.ux_textboxDescription.Name = "ux_textboxDescription";
            this.ux_textboxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxDescription.Size = new System.Drawing.Size(239, 59);
            this.ux_textboxDescription.TabIndex = 112;
            this.ux_textboxDescription.Tag = "description";
            // 
            // ux_textboxCopyrightInformation
            // 
            this.ux_textboxCopyrightInformation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCopyrightInformation.Location = new System.Drawing.Point(310, 143);
            this.ux_textboxCopyrightInformation.Multiline = true;
            this.ux_textboxCopyrightInformation.Name = "ux_textboxCopyrightInformation";
            this.ux_textboxCopyrightInformation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxCopyrightInformation.Size = new System.Drawing.Size(372, 60);
            this.ux_textboxCopyrightInformation.TabIndex = 31;
            this.ux_textboxCopyrightInformation.Tag = "copyright_information";
            // 
            // ux_labelDescription
            // 
            this.ux_labelDescription.AutoSize = true;
            this.ux_labelDescription.Location = new System.Drawing.Point(440, 6);
            this.ux_labelDescription.Name = "ux_labelDescription";
            this.ux_labelDescription.Size = new System.Drawing.Size(60, 13);
            this.ux_labelDescription.TabIndex = 113;
            this.ux_labelDescription.Tag = "description";
            this.ux_labelDescription.Text = "Description";
            // 
            // ux_labelInventoryNumber
            // 
            this.ux_labelInventoryNumber.AutoSize = true;
            this.ux_labelInventoryNumber.Location = new System.Drawing.Point(3, 6);
            this.ux_labelInventoryNumber.Name = "ux_labelInventoryNumber";
            this.ux_labelInventoryNumber.Size = new System.Drawing.Size(91, 13);
            this.ux_labelInventoryNumber.TabIndex = 17;
            this.ux_labelInventoryNumber.Tag = "inventory_id";
            this.ux_labelInventoryNumber.Text = "Inventory Number";
            // 
            // ux_textboxTitle
            // 
            this.ux_textboxTitle.Location = new System.Drawing.Point(218, 22);
            this.ux_textboxTitle.Multiline = true;
            this.ux_textboxTitle.Name = "ux_textboxTitle";
            this.ux_textboxTitle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxTitle.Size = new System.Drawing.Size(219, 58);
            this.ux_textboxTitle.TabIndex = 110;
            this.ux_textboxTitle.Tag = "title";
            // 
            // ux_textboxInventoryNumber
            // 
            this.ux_textboxInventoryNumber.BackColor = System.Drawing.Color.Plum;
            this.ux_textboxInventoryNumber.Location = new System.Drawing.Point(6, 22);
            this.ux_textboxInventoryNumber.Name = "ux_textboxInventoryNumber";
            this.ux_textboxInventoryNumber.Size = new System.Drawing.Size(206, 20);
            this.ux_textboxInventoryNumber.TabIndex = 16;
            this.ux_textboxInventoryNumber.Tag = "inventory_id";
            // 
            // ux_labelTitle
            // 
            this.ux_labelTitle.AutoSize = true;
            this.ux_labelTitle.Location = new System.Drawing.Point(215, 6);
            this.ux_labelTitle.Name = "ux_labelTitle";
            this.ux_labelTitle.Size = new System.Drawing.Size(27, 13);
            this.ux_labelTitle.TabIndex = 111;
            this.ux_labelTitle.Tag = "title";
            this.ux_labelTitle.Text = "Title";
            // 
            // ux_labelSortOrder
            // 
            this.ux_labelSortOrder.AutoSize = true;
            this.ux_labelSortOrder.Location = new System.Drawing.Point(3, 45);
            this.ux_labelSortOrder.Name = "ux_labelSortOrder";
            this.ux_labelSortOrder.Size = new System.Drawing.Size(55, 13);
            this.ux_labelSortOrder.TabIndex = 28;
            this.ux_labelSortOrder.Tag = "sort_order";
            this.ux_labelSortOrder.Text = "Sort Order";
            // 
            // ux_labelAttachCooperator
            // 
            this.ux_labelAttachCooperator.AutoSize = true;
            this.ux_labelAttachCooperator.Location = new System.Drawing.Point(307, 84);
            this.ux_labelAttachCooperator.Name = "ux_labelAttachCooperator";
            this.ux_labelAttachCooperator.Size = new System.Drawing.Size(93, 13);
            this.ux_labelAttachCooperator.TabIndex = 109;
            this.ux_labelAttachCooperator.Tag = "attach_cooperator_id";
            this.ux_labelAttachCooperator.Text = "Attach Cooperator";
            // 
            // ux_textboxSortOrder
            // 
            this.ux_textboxSortOrder.Location = new System.Drawing.Point(6, 61);
            this.ux_textboxSortOrder.Name = "ux_textboxSortOrder";
            this.ux_textboxSortOrder.Size = new System.Drawing.Size(60, 20);
            this.ux_textboxSortOrder.TabIndex = 27;
            this.ux_textboxSortOrder.Tag = "sort_order";
            // 
            // ux_textboxAttachCooperator
            // 
            this.ux_textboxAttachCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxAttachCooperator.Location = new System.Drawing.Point(310, 100);
            this.ux_textboxAttachCooperator.Name = "ux_textboxAttachCooperator";
            this.ux_textboxAttachCooperator.Size = new System.Drawing.Size(372, 20);
            this.ux_textboxAttachCooperator.TabIndex = 108;
            this.ux_textboxAttachCooperator.Tag = "attach_cooperator_id";
            // 
            // ux_labelNote
            // 
            this.ux_labelNote.AutoSize = true;
            this.ux_labelNote.Location = new System.Drawing.Point(6, 206);
            this.ux_labelNote.Name = "ux_labelNote";
            this.ux_labelNote.Size = new System.Drawing.Size(35, 13);
            this.ux_labelNote.TabIndex = 30;
            this.ux_labelNote.Tag = "note";
            this.ux_labelNote.Text = "Notes";
            // 
            // ux_comboboxAttachDateCode
            // 
            this.ux_comboboxAttachDateCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxAttachDateCode.FormattingEnabled = true;
            this.ux_comboboxAttachDateCode.Location = new System.Drawing.Point(139, 100);
            this.ux_comboboxAttachDateCode.Name = "ux_comboboxAttachDateCode";
            this.ux_comboboxAttachDateCode.Size = new System.Drawing.Size(165, 21);
            this.ux_comboboxAttachDateCode.TabIndex = 106;
            this.ux_comboboxAttachDateCode.Tag = "attach_date_code";
            // 
            // ux_textboxNote
            // 
            this.ux_textboxNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxNote.Location = new System.Drawing.Point(6, 222);
            this.ux_textboxNote.Multiline = true;
            this.ux_textboxNote.Name = "ux_textboxNote";
            this.ux_textboxNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxNote.Size = new System.Drawing.Size(676, 56);
            this.ux_textboxNote.TabIndex = 29;
            this.ux_textboxNote.Tag = "note";
            // 
            // ux_labelAttachDateCode
            // 
            this.ux_labelAttachDateCode.AutoSize = true;
            this.ux_labelAttachDateCode.Location = new System.Drawing.Point(136, 84);
            this.ux_labelAttachDateCode.Name = "ux_labelAttachDateCode";
            this.ux_labelAttachDateCode.Size = new System.Drawing.Size(92, 13);
            this.ux_labelAttachDateCode.TabIndex = 107;
            this.ux_labelAttachDateCode.Tag = "attach_date_code";
            this.ux_labelAttachDateCode.Text = "Attach Date Code";
            // 
            // ux_labelCopyrightInformation
            // 
            this.ux_labelCopyrightInformation.AutoSize = true;
            this.ux_labelCopyrightInformation.Location = new System.Drawing.Point(307, 128);
            this.ux_labelCopyrightInformation.Name = "ux_labelCopyrightInformation";
            this.ux_labelCopyrightInformation.Size = new System.Drawing.Size(106, 13);
            this.ux_labelCopyrightInformation.TabIndex = 32;
            this.ux_labelCopyrightInformation.Tag = "copyright_information";
            this.ux_labelCopyrightInformation.Text = "Copyright Information";
            // 
            // ux_textboxAttachDate
            // 
            this.ux_textboxAttachDate.Location = new System.Drawing.Point(6, 100);
            this.ux_textboxAttachDate.Name = "ux_textboxAttachDate";
            this.ux_textboxAttachDate.Size = new System.Drawing.Size(124, 20);
            this.ux_textboxAttachDate.TabIndex = 38;
            this.ux_textboxAttachDate.Tag = "attach_date";
            // 
            // ux_checkboxIsWebVisible
            // 
            this.ux_checkboxIsWebVisible.AutoSize = true;
            this.ux_checkboxIsWebVisible.Location = new System.Drawing.Point(81, 63);
            this.ux_checkboxIsWebVisible.Name = "ux_checkboxIsWebVisible";
            this.ux_checkboxIsWebVisible.Size = new System.Drawing.Size(99, 17);
            this.ux_checkboxIsWebVisible.TabIndex = 33;
            this.ux_checkboxIsWebVisible.TabStop = false;
            this.ux_checkboxIsWebVisible.Tag = "is_web_visible";
            this.ux_checkboxIsWebVisible.Text = "Is Web Visible?";
            this.ux_checkboxIsWebVisible.UseVisualStyleBackColor = true;
            // 
            // ux_labelAttachDate
            // 
            this.ux_labelAttachDate.AutoSize = true;
            this.ux_labelAttachDate.Location = new System.Drawing.Point(3, 84);
            this.ux_labelAttachDate.Name = "ux_labelAttachDate";
            this.ux_labelAttachDate.Size = new System.Drawing.Size(64, 13);
            this.ux_labelAttachDate.TabIndex = 39;
            this.ux_labelAttachDate.Tag = "attach_date";
            this.ux_labelAttachDate.Text = "Attach Date";
            // 
            // ux_labelCategoryCode
            // 
            this.ux_labelCategoryCode.AutoSize = true;
            this.ux_labelCategoryCode.Location = new System.Drawing.Point(170, 128);
            this.ux_labelCategoryCode.Name = "ux_labelCategoryCode";
            this.ux_labelCategoryCode.Size = new System.Drawing.Size(77, 13);
            this.ux_labelCategoryCode.TabIndex = 35;
            this.ux_labelCategoryCode.Tag = "category_code";
            this.ux_labelCategoryCode.Text = "Category Code";
            // 
            // ux_textboxContentType
            // 
            this.ux_textboxContentType.Location = new System.Drawing.Point(6, 183);
            this.ux_textboxContentType.Name = "ux_textboxContentType";
            this.ux_textboxContentType.Size = new System.Drawing.Size(295, 20);
            this.ux_textboxContentType.TabIndex = 36;
            this.ux_textboxContentType.Tag = "content_type";
            // 
            // ux_comboboxCategoryCode
            // 
            this.ux_comboboxCategoryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxCategoryCode.FormattingEnabled = true;
            this.ux_comboboxCategoryCode.Location = new System.Drawing.Point(173, 143);
            this.ux_comboboxCategoryCode.Name = "ux_comboboxCategoryCode";
            this.ux_comboboxCategoryCode.Size = new System.Drawing.Size(128, 21);
            this.ux_comboboxCategoryCode.TabIndex = 34;
            this.ux_comboboxCategoryCode.Tag = "category_code";
            // 
            // ux_labelContentType
            // 
            this.ux_labelContentType.AutoSize = true;
            this.ux_labelContentType.Location = new System.Drawing.Point(6, 167);
            this.ux_labelContentType.Name = "ux_labelContentType";
            this.ux_labelContentType.Size = new System.Drawing.Size(71, 13);
            this.ux_labelContentType.TabIndex = 37;
            this.ux_labelContentType.Tag = "content_type";
            this.ux_labelContentType.Text = "Content Type";
            // 
            // ux_tabpageGridView
            // 
            this.ux_tabpageGridView.Controls.Add(this.ux_datagridviewAttachments);
            this.ux_tabpageGridView.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageGridView.Name = "ux_tabpageGridView";
            this.ux_tabpageGridView.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageGridView.Size = new System.Drawing.Size(681, 279);
            this.ux_tabpageGridView.TabIndex = 1;
            this.ux_tabpageGridView.Text = "Grid View";
            this.ux_tabpageGridView.UseVisualStyleBackColor = true;
            // 
            // ux_openfiledialog
            // 
            this.ux_openfiledialog.Filter = "All files (*.*)|*.*";
            // 
            // ux_progressbar
            // 
            this.ux_progressbar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_progressbar.Location = new System.Drawing.Point(506, 27);
            this.ux_progressbar.Name = "ux_progressbar";
            this.ux_progressbar.Size = new System.Drawing.Size(236, 23);
            this.ux_progressbar.TabIndex = 3;
            this.ux_progressbar.Visible = false;
            // 
            // ux_labelProgressValue
            // 
            this.ux_labelProgressValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelProgressValue.AutoSize = true;
            this.ux_labelProgressValue.Location = new System.Drawing.Point(748, 32);
            this.ux_labelProgressValue.Name = "ux_labelProgressValue";
            this.ux_labelProgressValue.Size = new System.Drawing.Size(21, 13);
            this.ux_labelProgressValue.TabIndex = 4;
            this.ux_labelProgressValue.Text = "0%";
            this.ux_labelProgressValue.Visible = false;
            // 
            // ux_labelProgressDescription
            // 
            this.ux_labelProgressDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelProgressDescription.Location = new System.Drawing.Point(12, 27);
            this.ux_labelProgressDescription.Name = "ux_labelProgressDescription";
            this.ux_labelProgressDescription.Size = new System.Drawing.Size(488, 23);
            this.ux_labelProgressDescription.TabIndex = 5;
            this.ux_labelProgressDescription.Text = "description";
            this.ux_labelProgressDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ux_labelProgressDescription.Visible = false;
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.Location = new System.Drawing.Point(786, 27);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(90, 23);
            this.ux_buttonSave.TabIndex = 6;
            this.ux_buttonSave.Text = "Save";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonSaveAndExit
            // 
            this.ux_buttonSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveAndExit.Location = new System.Drawing.Point(882, 27);
            this.ux_buttonSaveAndExit.Name = "ux_buttonSaveAndExit";
            this.ux_buttonSaveAndExit.Size = new System.Drawing.Size(90, 23);
            this.ux_buttonSaveAndExit.TabIndex = 7;
            this.ux_buttonSaveAndExit.Text = "Save and Exit";
            this.ux_buttonSaveAndExit.UseVisualStyleBackColor = true;
            this.ux_buttonSaveAndExit.Click += new System.EventHandler(this.ux_buttonSaveAndExit_Click);
            // 
            // ux_menustripMain
            // 
            this.ux_menustripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.ux_menustripMain.Location = new System.Drawing.Point(0, 0);
            this.ux_menustripMain.Name = "ux_menustripMain";
            this.ux_menustripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ux_menustripMain.Size = new System.Drawing.Size(984, 24);
            this.ux_menustripMain.TabIndex = 8;
            this.ux_menustripMain.Text = "ux_menustripMain";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // InventoryAttachmentWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 649);
            this.Controls.Add(this.ux_buttonSaveAndExit);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_labelProgressDescription);
            this.Controls.Add(this.ux_labelProgressValue);
            this.Controls.Add(this.ux_progressbar);
            this.Controls.Add(this.ux_splitcontainerMain);
            this.Controls.Add(this.ux_menustripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.ux_menustripMain;
            this.Name = "InventoryAttachmentWizard";
            this.Text = "Inventory Attachment Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InventoryAttachmentWizard_FormClosing);
            this.Load += new System.EventHandler(this.InventoryAttachmentWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachments)).EndInit();
            this.ux_splitcontainerMain.Panel1.ResumeLayout(false);
            this.ux_splitcontainerMain.Panel1.PerformLayout();
            this.ux_splitcontainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerMain)).EndInit();
            this.ux_splitcontainerMain.ResumeLayout(false);
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.ResumeLayout(false);
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerAttachmentDataDisplay)).EndInit();
            this.ux_splitcontainerAttachmentDataDisplay.ResumeLayout(false);
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.ux_tabpageFormView.ResumeLayout(false);
            this.ux_panelFormView.ResumeLayout(false);
            this.ux_panelFormView.PerformLayout();
            this.ux_tabpageGridView.ResumeLayout(false);
            this.ux_menustripMain.ResumeLayout(false);
            this.ux_menustripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewAttachments;
        private System.Windows.Forms.Button ux_buttonBrowse;
        private System.Windows.Forms.SplitContainer ux_splitcontainerMain;
        private System.Windows.Forms.SplitContainer ux_splitcontainerAttachmentDataDisplay;
        private System.Windows.Forms.OpenFileDialog ux_openfiledialog;
        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.TabPage ux_tabpageFormView;
        private System.Windows.Forms.TabPage ux_tabpageGridView;
        private System.Windows.Forms.TreeView ux_treeviewInventoryList;
        private System.Windows.Forms.ListView ux_listviewAttachments;
        private System.Windows.Forms.ProgressBar ux_progressbar;
        private System.Windows.Forms.Label ux_labelProgressValue;
        private System.Windows.Forms.Label ux_labelProgressDescription;
        private System.Windows.Forms.TextBox ux_textboxInventoryNumber;
        private System.Windows.Forms.Label ux_labelInventoryNumber;
        private System.Windows.Forms.TextBox ux_textboxSortOrder;
        private System.Windows.Forms.Label ux_labelSortOrder;
        private System.Windows.Forms.TextBox ux_textboxCopyrightInformation;
        private System.Windows.Forms.Label ux_labelCopyrightInformation;
        private System.Windows.Forms.TextBox ux_textboxNote;
        private System.Windows.Forms.Label ux_labelNote;
        private System.Windows.Forms.CheckBox ux_checkboxIsWebVisible;
        private System.Windows.Forms.ComboBox ux_comboboxCategoryCode;
        private System.Windows.Forms.Label ux_labelCategoryCode;
        private System.Windows.Forms.TextBox ux_textboxContentType;
        private System.Windows.Forms.Label ux_labelContentType;
        private System.Windows.Forms.TextBox ux_textboxAttachDate;
        private System.Windows.Forms.Label ux_labelAttachDate;
        private System.Windows.Forms.ComboBox ux_comboboxAttachDateCode;
        private System.Windows.Forms.Label ux_labelAttachDateCode;
        private System.Windows.Forms.TextBox ux_textboxDescription;
        private System.Windows.Forms.Label ux_labelDescription;
        private System.Windows.Forms.TextBox ux_textboxTitle;
        private System.Windows.Forms.Label ux_labelTitle;
        private System.Windows.Forms.Label ux_labelAttachCooperator;
        private System.Windows.Forms.TextBox ux_textboxAttachCooperator;
        private System.Windows.Forms.Panel ux_panelFormView;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonSaveAndExit;
        private System.Windows.Forms.MenuStrip ux_menustripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.CheckBox ux_checkboxViewExistingAttachments;
        private System.Windows.Forms.Label ux_labelDescriptionCode;
        private System.Windows.Forms.ComboBox ux_comboboxDescriptionCode;
    }
}


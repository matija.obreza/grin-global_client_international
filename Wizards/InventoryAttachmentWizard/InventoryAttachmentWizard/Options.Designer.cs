﻿namespace InventoryAttachmentWizard
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_labelIncludeFilter = new System.Windows.Forms.Label();
            this.ux_labelExcludeFilter = new System.Windows.Forms.Label();
            this.ux_textboxIncludeFilter = new System.Windows.Forms.TextBox();
            this.ux_textboxExcludeFilter = new System.Windows.Forms.TextBox();
            this.ux_groupboxAttachmentFileFilters = new System.Windows.Forms.GroupBox();
            this.ux_buttonOK = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_groupboxFileNameAutomation = new System.Windows.Forms.GroupBox();
            this.ux_groupboxTokenMapping = new System.Windows.Forms.GroupBox();
            this.ux_textboxContent = new System.Windows.Forms.TextBox();
            this.ux_labelContent = new System.Windows.Forms.Label();
            this.ux_textboxDate = new System.Windows.Forms.TextBox();
            this.ux_labelDate = new System.Windows.Forms.Label();
            this.ux_textboxFormType = new System.Windows.Forms.TextBox();
            this.ux_labelFormType = new System.Windows.Forms.Label();
            this.ux_textboxSuffix = new System.Windows.Forms.TextBox();
            this.ux_labelSuffix = new System.Windows.Forms.Label();
            this.ux_textboxNumber = new System.Windows.Forms.TextBox();
            this.ux_labelNumber = new System.Windows.Forms.Label();
            this.ux_textboxPrefix = new System.Windows.Forms.TextBox();
            this.ux_labelPrefix = new System.Windows.Forms.Label();
            this.ux_groupboxFixedFieldTokens = new System.Windows.Forms.GroupBox();
            this.ux_textboxFixedToken12 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken11 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken10 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken9 = new System.Windows.Forms.TextBox();
            this.ux_labelFixedToken12 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken11 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken10 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken9 = new System.Windows.Forms.Label();
            this.ux_textboxFixedToken8 = new System.Windows.Forms.TextBox();
            this.ux_labelFixedToken8 = new System.Windows.Forms.Label();
            this.ux_textboxFixedToken7 = new System.Windows.Forms.TextBox();
            this.ux_labelFixedToken7 = new System.Windows.Forms.Label();
            this.ux_textboxFixedToken6 = new System.Windows.Forms.TextBox();
            this.ux_labelFixedToken6 = new System.Windows.Forms.Label();
            this.ux_textboxFixedToken5 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken4 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken3 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken2 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken1 = new System.Windows.Forms.TextBox();
            this.ux_labelFixedToken5 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken4 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken3 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken2 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken1 = new System.Windows.Forms.Label();
            this.ux_checkboxCaseSensitive = new System.Windows.Forms.CheckBox();
            this.ux_groupboxDelimiters = new System.Windows.Forms.GroupBox();
            this.ux_checkboxDash = new System.Windows.Forms.CheckBox();
            this.ux_textboxOther = new System.Windows.Forms.TextBox();
            this.ux_checkboxUnderscore = new System.Windows.Forms.CheckBox();
            this.ux_checkboxOther = new System.Windows.Forms.CheckBox();
            this.ux_checkboxSpace = new System.Windows.Forms.CheckBox();
            this.ux_radiobuttonFixedFieldParse = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonDelimitedParse = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonSmartParse = new System.Windows.Forms.RadioButton();
            this.ux_groupboxAttachmentFileFilters.SuspendLayout();
            this.ux_groupboxFileNameAutomation.SuspendLayout();
            this.ux_groupboxTokenMapping.SuspendLayout();
            this.ux_groupboxFixedFieldTokens.SuspendLayout();
            this.ux_groupboxDelimiters.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_labelIncludeFilter
            // 
            this.ux_labelIncludeFilter.AutoSize = true;
            this.ux_labelIncludeFilter.Location = new System.Drawing.Point(6, 16);
            this.ux_labelIncludeFilter.Name = "ux_labelIncludeFilter";
            this.ux_labelIncludeFilter.Size = new System.Drawing.Size(362, 13);
            this.ux_labelIncludeFilter.TabIndex = 0;
            this.ux_labelIncludeFilter.Text = "Include:     (e.g. *.jpg; *.png; *.gif; *.xlsx; *.docx; *.pptx; *.pdf; *.txt; *.r" +
    "tf; *.zip)";
            // 
            // ux_labelExcludeFilter
            // 
            this.ux_labelExcludeFilter.AutoSize = true;
            this.ux_labelExcludeFilter.Location = new System.Drawing.Point(6, 55);
            this.ux_labelExcludeFilter.Name = "ux_labelExcludeFilter";
            this.ux_labelExcludeFilter.Size = new System.Drawing.Size(165, 13);
            this.ux_labelExcludeFilter.TabIndex = 1;
            this.ux_labelExcludeFilter.Text = "Exclude:     (e.g. *.tif; *.tiff; *_t.jpg)";
            // 
            // ux_textboxIncludeFilter
            // 
            this.ux_textboxIncludeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxIncludeFilter.Location = new System.Drawing.Point(9, 32);
            this.ux_textboxIncludeFilter.Name = "ux_textboxIncludeFilter";
            this.ux_textboxIncludeFilter.Size = new System.Drawing.Size(423, 20);
            this.ux_textboxIncludeFilter.TabIndex = 2;
            // 
            // ux_textboxExcludeFilter
            // 
            this.ux_textboxExcludeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxExcludeFilter.Location = new System.Drawing.Point(9, 71);
            this.ux_textboxExcludeFilter.Name = "ux_textboxExcludeFilter";
            this.ux_textboxExcludeFilter.Size = new System.Drawing.Size(423, 20);
            this.ux_textboxExcludeFilter.TabIndex = 3;
            // 
            // ux_groupboxAttachmentFileFilters
            // 
            this.ux_groupboxAttachmentFileFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_labelIncludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_textboxExcludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_textboxIncludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_labelExcludeFilter);
            this.ux_groupboxAttachmentFileFilters.Location = new System.Drawing.Point(13, 12);
            this.ux_groupboxAttachmentFileFilters.Name = "ux_groupboxAttachmentFileFilters";
            this.ux_groupboxAttachmentFileFilters.Size = new System.Drawing.Size(438, 103);
            this.ux_groupboxAttachmentFileFilters.TabIndex = 4;
            this.ux_groupboxAttachmentFileFilters.TabStop = false;
            this.ux_groupboxAttachmentFileFilters.Text = "Attachment File Filters";
            // 
            // ux_buttonOK
            // 
            this.ux_buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonOK.Location = new System.Drawing.Point(287, 406);
            this.ux_buttonOK.Name = "ux_buttonOK";
            this.ux_buttonOK.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonOK.TabIndex = 5;
            this.ux_buttonOK.Text = "OK";
            this.ux_buttonOK.UseVisualStyleBackColor = true;
            this.ux_buttonOK.Click += new System.EventHandler(this.ux_buttonOK_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(376, 406);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 6;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ux_groupboxFileNameAutomation
            // 
            this.ux_groupboxFileNameAutomation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_groupboxTokenMapping);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_groupboxFixedFieldTokens);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_checkboxCaseSensitive);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_groupboxDelimiters);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonFixedFieldParse);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonDelimitedParse);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonSmartParse);
            this.ux_groupboxFileNameAutomation.Location = new System.Drawing.Point(13, 122);
            this.ux_groupboxFileNameAutomation.Name = "ux_groupboxFileNameAutomation";
            this.ux_groupboxFileNameAutomation.Size = new System.Drawing.Size(438, 279);
            this.ux_groupboxFileNameAutomation.TabIndex = 7;
            this.ux_groupboxFileNameAutomation.TabStop = false;
            this.ux_groupboxFileNameAutomation.Text = "File Name Automation";
            // 
            // ux_groupboxTokenMapping
            // 
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxContent);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelContent);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxDate);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelDate);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxFormType);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelFormType);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxSuffix);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelSuffix);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxNumber);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelNumber);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_textboxPrefix);
            this.ux_groupboxTokenMapping.Controls.Add(this.ux_labelPrefix);
            this.ux_groupboxTokenMapping.Location = new System.Drawing.Point(9, 201);
            this.ux_groupboxTokenMapping.Name = "ux_groupboxTokenMapping";
            this.ux_groupboxTokenMapping.Size = new System.Drawing.Size(423, 70);
            this.ux_groupboxTokenMapping.TabIndex = 11;
            this.ux_groupboxTokenMapping.TabStop = false;
            this.ux_groupboxTokenMapping.Text = "Position of Token in File Name";
            // 
            // ux_textboxContent
            // 
            this.ux_textboxContent.Location = new System.Drawing.Point(131, 41);
            this.ux_textboxContent.Name = "ux_textboxContent";
            this.ux_textboxContent.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxContent.TabIndex = 17;
            this.ux_textboxContent.Text = "6";
            // 
            // ux_labelContent
            // 
            this.ux_labelContent.AutoSize = true;
            this.ux_labelContent.Location = new System.Drawing.Point(78, 44);
            this.ux_labelContent.Name = "ux_labelContent";
            this.ux_labelContent.Size = new System.Drawing.Size(47, 13);
            this.ux_labelContent.TabIndex = 16;
            this.ux_labelContent.Text = "Content:";
            // 
            // ux_textboxDate
            // 
            this.ux_textboxDate.Location = new System.Drawing.Point(48, 41);
            this.ux_textboxDate.Name = "ux_textboxDate";
            this.ux_textboxDate.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxDate.TabIndex = 15;
            this.ux_textboxDate.Text = "5";
            // 
            // ux_labelDate
            // 
            this.ux_labelDate.AutoSize = true;
            this.ux_labelDate.Location = new System.Drawing.Point(6, 44);
            this.ux_labelDate.Name = "ux_labelDate";
            this.ux_labelDate.Size = new System.Drawing.Size(33, 13);
            this.ux_labelDate.TabIndex = 14;
            this.ux_labelDate.Text = "Date:";
            // 
            // ux_textboxFormType
            // 
            this.ux_textboxFormType.Location = new System.Drawing.Point(295, 16);
            this.ux_textboxFormType.Name = "ux_textboxFormType";
            this.ux_textboxFormType.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFormType.TabIndex = 13;
            this.ux_textboxFormType.Text = "4";
            // 
            // ux_labelFormType
            // 
            this.ux_labelFormType.AutoSize = true;
            this.ux_labelFormType.Location = new System.Drawing.Point(232, 19);
            this.ux_labelFormType.Name = "ux_labelFormType";
            this.ux_labelFormType.Size = new System.Drawing.Size(57, 13);
            this.ux_labelFormType.TabIndex = 12;
            this.ux_labelFormType.Text = "FormType:";
            // 
            // ux_textboxSuffix
            // 
            this.ux_textboxSuffix.Location = new System.Drawing.Point(202, 16);
            this.ux_textboxSuffix.Name = "ux_textboxSuffix";
            this.ux_textboxSuffix.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxSuffix.TabIndex = 11;
            this.ux_textboxSuffix.Text = "3";
            // 
            // ux_labelSuffix
            // 
            this.ux_labelSuffix.AutoSize = true;
            this.ux_labelSuffix.Location = new System.Drawing.Point(160, 19);
            this.ux_labelSuffix.Name = "ux_labelSuffix";
            this.ux_labelSuffix.Size = new System.Drawing.Size(36, 13);
            this.ux_labelSuffix.TabIndex = 10;
            this.ux_labelSuffix.Text = "Suffix:";
            // 
            // ux_textboxNumber
            // 
            this.ux_textboxNumber.Location = new System.Drawing.Point(130, 16);
            this.ux_textboxNumber.Name = "ux_textboxNumber";
            this.ux_textboxNumber.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxNumber.TabIndex = 9;
            this.ux_textboxNumber.Text = "2";
            // 
            // ux_labelNumber
            // 
            this.ux_labelNumber.AutoSize = true;
            this.ux_labelNumber.Location = new System.Drawing.Point(77, 19);
            this.ux_labelNumber.Name = "ux_labelNumber";
            this.ux_labelNumber.Size = new System.Drawing.Size(47, 13);
            this.ux_labelNumber.TabIndex = 8;
            this.ux_labelNumber.Text = "Number:";
            // 
            // ux_textboxPrefix
            // 
            this.ux_textboxPrefix.Location = new System.Drawing.Point(47, 16);
            this.ux_textboxPrefix.Name = "ux_textboxPrefix";
            this.ux_textboxPrefix.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxPrefix.TabIndex = 7;
            this.ux_textboxPrefix.Text = "1";
            // 
            // ux_labelPrefix
            // 
            this.ux_labelPrefix.AutoSize = true;
            this.ux_labelPrefix.Location = new System.Drawing.Point(5, 19);
            this.ux_labelPrefix.Name = "ux_labelPrefix";
            this.ux_labelPrefix.Size = new System.Drawing.Size(36, 13);
            this.ux_labelPrefix.TabIndex = 6;
            this.ux_labelPrefix.Text = "Prefix:";
            // 
            // ux_groupboxFixedFieldTokens
            // 
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken12);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken11);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken10);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken9);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken12);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken11);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken10);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken9);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken8);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken8);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken7);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken7);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken6);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken6);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken5);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken4);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken3);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken2);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken1);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken5);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken4);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken3);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken2);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken1);
            this.ux_groupboxFixedFieldTokens.Enabled = false;
            this.ux_groupboxFixedFieldTokens.Location = new System.Drawing.Point(120, 125);
            this.ux_groupboxFixedFieldTokens.Name = "ux_groupboxFixedFieldTokens";
            this.ux_groupboxFixedFieldTokens.Size = new System.Drawing.Size(312, 70);
            this.ux_groupboxFixedFieldTokens.TabIndex = 10;
            this.ux_groupboxFixedFieldTokens.TabStop = false;
            this.ux_groupboxFixedFieldTokens.Text = "Number of Characters in Each Token";
            // 
            // ux_textboxFixedToken12
            // 
            this.ux_textboxFixedToken12.Location = new System.Drawing.Point(277, 43);
            this.ux_textboxFixedToken12.Name = "ux_textboxFixedToken12";
            this.ux_textboxFixedToken12.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken12.TabIndex = 24;
            this.ux_textboxFixedToken12.Text = "0";
            // 
            // ux_textboxFixedToken11
            // 
            this.ux_textboxFixedToken11.Location = new System.Drawing.Point(226, 43);
            this.ux_textboxFixedToken11.Name = "ux_textboxFixedToken11";
            this.ux_textboxFixedToken11.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken11.TabIndex = 23;
            this.ux_textboxFixedToken11.Text = "0";
            // 
            // ux_textboxFixedToken10
            // 
            this.ux_textboxFixedToken10.Location = new System.Drawing.Point(176, 43);
            this.ux_textboxFixedToken10.Name = "ux_textboxFixedToken10";
            this.ux_textboxFixedToken10.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken10.TabIndex = 22;
            this.ux_textboxFixedToken10.Text = "0";
            // 
            // ux_textboxFixedToken9
            // 
            this.ux_textboxFixedToken9.Location = new System.Drawing.Point(125, 43);
            this.ux_textboxFixedToken9.Name = "ux_textboxFixedToken9";
            this.ux_textboxFixedToken9.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken9.TabIndex = 21;
            this.ux_textboxFixedToken9.Text = "0";
            // 
            // ux_labelFixedToken12
            // 
            this.ux_labelFixedToken12.AutoSize = true;
            this.ux_labelFixedToken12.Location = new System.Drawing.Point(255, 46);
            this.ux_labelFixedToken12.Name = "ux_labelFixedToken12";
            this.ux_labelFixedToken12.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken12.TabIndex = 19;
            this.ux_labelFixedToken12.Text = "12:";
            // 
            // ux_labelFixedToken11
            // 
            this.ux_labelFixedToken11.AutoSize = true;
            this.ux_labelFixedToken11.Location = new System.Drawing.Point(206, 46);
            this.ux_labelFixedToken11.Name = "ux_labelFixedToken11";
            this.ux_labelFixedToken11.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken11.TabIndex = 18;
            this.ux_labelFixedToken11.Text = "11:";
            // 
            // ux_labelFixedToken10
            // 
            this.ux_labelFixedToken10.AutoSize = true;
            this.ux_labelFixedToken10.Location = new System.Drawing.Point(155, 46);
            this.ux_labelFixedToken10.Name = "ux_labelFixedToken10";
            this.ux_labelFixedToken10.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken10.TabIndex = 17;
            this.ux_labelFixedToken10.Text = "10:";
            // 
            // ux_labelFixedToken9
            // 
            this.ux_labelFixedToken9.AutoSize = true;
            this.ux_labelFixedToken9.Location = new System.Drawing.Point(110, 46);
            this.ux_labelFixedToken9.Name = "ux_labelFixedToken9";
            this.ux_labelFixedToken9.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken9.TabIndex = 16;
            this.ux_labelFixedToken9.Text = "9:";
            // 
            // ux_textboxFixedToken8
            // 
            this.ux_textboxFixedToken8.Location = new System.Drawing.Point(73, 43);
            this.ux_textboxFixedToken8.Name = "ux_textboxFixedToken8";
            this.ux_textboxFixedToken8.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken8.TabIndex = 15;
            this.ux_textboxFixedToken8.Text = "0";
            // 
            // ux_labelFixedToken8
            // 
            this.ux_labelFixedToken8.AutoSize = true;
            this.ux_labelFixedToken8.Location = new System.Drawing.Point(57, 46);
            this.ux_labelFixedToken8.Name = "ux_labelFixedToken8";
            this.ux_labelFixedToken8.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken8.TabIndex = 14;
            this.ux_labelFixedToken8.Text = "8:";
            // 
            // ux_textboxFixedToken7
            // 
            this.ux_textboxFixedToken7.Location = new System.Drawing.Point(21, 43);
            this.ux_textboxFixedToken7.Name = "ux_textboxFixedToken7";
            this.ux_textboxFixedToken7.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken7.TabIndex = 13;
            this.ux_textboxFixedToken7.Text = "0";
            // 
            // ux_labelFixedToken7
            // 
            this.ux_labelFixedToken7.AutoSize = true;
            this.ux_labelFixedToken7.Location = new System.Drawing.Point(5, 46);
            this.ux_labelFixedToken7.Name = "ux_labelFixedToken7";
            this.ux_labelFixedToken7.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken7.TabIndex = 12;
            this.ux_labelFixedToken7.Text = "7:";
            // 
            // ux_textboxFixedToken6
            // 
            this.ux_textboxFixedToken6.Location = new System.Drawing.Point(277, 17);
            this.ux_textboxFixedToken6.Name = "ux_textboxFixedToken6";
            this.ux_textboxFixedToken6.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken6.TabIndex = 11;
            this.ux_textboxFixedToken6.Text = "0";
            // 
            // ux_labelFixedToken6
            // 
            this.ux_labelFixedToken6.AutoSize = true;
            this.ux_labelFixedToken6.Location = new System.Drawing.Point(262, 20);
            this.ux_labelFixedToken6.Name = "ux_labelFixedToken6";
            this.ux_labelFixedToken6.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken6.TabIndex = 10;
            this.ux_labelFixedToken6.Text = "6:";
            // 
            // ux_textboxFixedToken5
            // 
            this.ux_textboxFixedToken5.Location = new System.Drawing.Point(226, 17);
            this.ux_textboxFixedToken5.Name = "ux_textboxFixedToken5";
            this.ux_textboxFixedToken5.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken5.TabIndex = 9;
            this.ux_textboxFixedToken5.Text = "0";
            // 
            // ux_textboxFixedToken4
            // 
            this.ux_textboxFixedToken4.Location = new System.Drawing.Point(176, 17);
            this.ux_textboxFixedToken4.Name = "ux_textboxFixedToken4";
            this.ux_textboxFixedToken4.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken4.TabIndex = 8;
            this.ux_textboxFixedToken4.Text = "0";
            // 
            // ux_textboxFixedToken3
            // 
            this.ux_textboxFixedToken3.Location = new System.Drawing.Point(124, 17);
            this.ux_textboxFixedToken3.Name = "ux_textboxFixedToken3";
            this.ux_textboxFixedToken3.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken3.TabIndex = 7;
            this.ux_textboxFixedToken3.Text = "0";
            // 
            // ux_textboxFixedToken2
            // 
            this.ux_textboxFixedToken2.Location = new System.Drawing.Point(72, 17);
            this.ux_textboxFixedToken2.Name = "ux_textboxFixedToken2";
            this.ux_textboxFixedToken2.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken2.TabIndex = 6;
            this.ux_textboxFixedToken2.Text = "0";
            // 
            // ux_textboxFixedToken1
            // 
            this.ux_textboxFixedToken1.Location = new System.Drawing.Point(21, 17);
            this.ux_textboxFixedToken1.Name = "ux_textboxFixedToken1";
            this.ux_textboxFixedToken1.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken1.TabIndex = 5;
            this.ux_textboxFixedToken1.Text = "0";
            // 
            // ux_labelFixedToken5
            // 
            this.ux_labelFixedToken5.AutoSize = true;
            this.ux_labelFixedToken5.Location = new System.Drawing.Point(210, 20);
            this.ux_labelFixedToken5.Name = "ux_labelFixedToken5";
            this.ux_labelFixedToken5.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken5.TabIndex = 4;
            this.ux_labelFixedToken5.Text = "5:";
            // 
            // ux_labelFixedToken4
            // 
            this.ux_labelFixedToken4.AutoSize = true;
            this.ux_labelFixedToken4.Location = new System.Drawing.Point(160, 20);
            this.ux_labelFixedToken4.Name = "ux_labelFixedToken4";
            this.ux_labelFixedToken4.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken4.TabIndex = 3;
            this.ux_labelFixedToken4.Text = "4:";
            // 
            // ux_labelFixedToken3
            // 
            this.ux_labelFixedToken3.AutoSize = true;
            this.ux_labelFixedToken3.Location = new System.Drawing.Point(108, 20);
            this.ux_labelFixedToken3.Name = "ux_labelFixedToken3";
            this.ux_labelFixedToken3.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken3.TabIndex = 2;
            this.ux_labelFixedToken3.Text = "3:";
            // 
            // ux_labelFixedToken2
            // 
            this.ux_labelFixedToken2.AutoSize = true;
            this.ux_labelFixedToken2.Location = new System.Drawing.Point(57, 20);
            this.ux_labelFixedToken2.Name = "ux_labelFixedToken2";
            this.ux_labelFixedToken2.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken2.TabIndex = 1;
            this.ux_labelFixedToken2.Text = "2:";
            // 
            // ux_labelFixedToken1
            // 
            this.ux_labelFixedToken1.AutoSize = true;
            this.ux_labelFixedToken1.Location = new System.Drawing.Point(6, 20);
            this.ux_labelFixedToken1.Name = "ux_labelFixedToken1";
            this.ux_labelFixedToken1.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken1.TabIndex = 0;
            this.ux_labelFixedToken1.Text = "1:";
            // 
            // ux_checkboxCaseSensitive
            // 
            this.ux_checkboxCaseSensitive.AutoSize = true;
            this.ux_checkboxCaseSensitive.Location = new System.Drawing.Point(126, 20);
            this.ux_checkboxCaseSensitive.Name = "ux_checkboxCaseSensitive";
            this.ux_checkboxCaseSensitive.Size = new System.Drawing.Size(96, 17);
            this.ux_checkboxCaseSensitive.TabIndex = 9;
            this.ux_checkboxCaseSensitive.Text = "Case Sensitive";
            this.ux_checkboxCaseSensitive.UseVisualStyleBackColor = true;
            // 
            // ux_groupboxDelimiters
            // 
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxDash);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_textboxOther);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxUnderscore);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxOther);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxSpace);
            this.ux_groupboxDelimiters.Enabled = false;
            this.ux_groupboxDelimiters.Location = new System.Drawing.Point(120, 52);
            this.ux_groupboxDelimiters.Name = "ux_groupboxDelimiters";
            this.ux_groupboxDelimiters.Size = new System.Drawing.Size(312, 67);
            this.ux_groupboxDelimiters.TabIndex = 8;
            this.ux_groupboxDelimiters.TabStop = false;
            this.ux_groupboxDelimiters.Text = "Token Delimiters";
            // 
            // ux_checkboxDash
            // 
            this.ux_checkboxDash.AutoSize = true;
            this.ux_checkboxDash.Location = new System.Drawing.Point(114, 19);
            this.ux_checkboxDash.Name = "ux_checkboxDash";
            this.ux_checkboxDash.Size = new System.Drawing.Size(51, 17);
            this.ux_checkboxDash.TabIndex = 5;
            this.ux_checkboxDash.Text = "Dash";
            this.ux_checkboxDash.UseVisualStyleBackColor = true;
            // 
            // ux_textboxOther
            // 
            this.ux_textboxOther.Enabled = false;
            this.ux_textboxOther.Location = new System.Drawing.Point(171, 41);
            this.ux_textboxOther.MaxLength = 1;
            this.ux_textboxOther.Name = "ux_textboxOther";
            this.ux_textboxOther.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxOther.TabIndex = 7;
            // 
            // ux_checkboxUnderscore
            // 
            this.ux_checkboxUnderscore.AutoSize = true;
            this.ux_checkboxUnderscore.Checked = true;
            this.ux_checkboxUnderscore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxUnderscore.Location = new System.Drawing.Point(6, 19);
            this.ux_checkboxUnderscore.Name = "ux_checkboxUnderscore";
            this.ux_checkboxUnderscore.Size = new System.Drawing.Size(81, 17);
            this.ux_checkboxUnderscore.TabIndex = 3;
            this.ux_checkboxUnderscore.Text = "Underscore";
            this.ux_checkboxUnderscore.UseVisualStyleBackColor = true;
            // 
            // ux_checkboxOther
            // 
            this.ux_checkboxOther.AutoSize = true;
            this.ux_checkboxOther.Location = new System.Drawing.Point(113, 42);
            this.ux_checkboxOther.Name = "ux_checkboxOther";
            this.ux_checkboxOther.Size = new System.Drawing.Size(52, 17);
            this.ux_checkboxOther.TabIndex = 6;
            this.ux_checkboxOther.Text = "Other";
            this.ux_checkboxOther.UseVisualStyleBackColor = true;
            // 
            // ux_checkboxSpace
            // 
            this.ux_checkboxSpace.AutoSize = true;
            this.ux_checkboxSpace.Checked = true;
            this.ux_checkboxSpace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxSpace.Location = new System.Drawing.Point(6, 43);
            this.ux_checkboxSpace.Name = "ux_checkboxSpace";
            this.ux_checkboxSpace.Size = new System.Drawing.Size(57, 17);
            this.ux_checkboxSpace.TabIndex = 4;
            this.ux_checkboxSpace.Text = "Space";
            this.ux_checkboxSpace.UseVisualStyleBackColor = true;
            // 
            // ux_radiobuttonFixedFieldParse
            // 
            this.ux_radiobuttonFixedFieldParse.AutoSize = true;
            this.ux_radiobuttonFixedFieldParse.Location = new System.Drawing.Point(6, 125);
            this.ux_radiobuttonFixedFieldParse.Name = "ux_radiobuttonFixedFieldParse";
            this.ux_radiobuttonFixedFieldParse.Size = new System.Drawing.Size(113, 17);
            this.ux_radiobuttonFixedFieldParse.TabIndex = 2;
            this.ux_radiobuttonFixedFieldParse.Text = "Fixed Field Parsing";
            this.ux_radiobuttonFixedFieldParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonFixedFieldParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonFixedFieldParse_CheckedChanged);
            // 
            // ux_radiobuttonDelimitedParse
            // 
            this.ux_radiobuttonDelimitedParse.AutoSize = true;
            this.ux_radiobuttonDelimitedParse.Location = new System.Drawing.Point(6, 52);
            this.ux_radiobuttonDelimitedParse.Name = "ux_radiobuttonDelimitedParse";
            this.ux_radiobuttonDelimitedParse.Size = new System.Drawing.Size(106, 17);
            this.ux_radiobuttonDelimitedParse.TabIndex = 1;
            this.ux_radiobuttonDelimitedParse.Text = "Delimited Parsing";
            this.ux_radiobuttonDelimitedParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonDelimitedParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonDelimitedParse_CheckedChanged);
            // 
            // ux_radiobuttonSmartParse
            // 
            this.ux_radiobuttonSmartParse.AutoSize = true;
            this.ux_radiobuttonSmartParse.Checked = true;
            this.ux_radiobuttonSmartParse.Location = new System.Drawing.Point(6, 19);
            this.ux_radiobuttonSmartParse.Name = "ux_radiobuttonSmartParse";
            this.ux_radiobuttonSmartParse.Size = new System.Drawing.Size(90, 17);
            this.ux_radiobuttonSmartParse.TabIndex = 0;
            this.ux_radiobuttonSmartParse.TabStop = true;
            this.ux_radiobuttonSmartParse.Text = "Smart Parsing";
            this.ux_radiobuttonSmartParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonSmartParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonSmartParse_CheckedChanged);
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 441);
            this.Controls.Add(this.ux_groupboxFileNameAutomation);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonOK);
            this.Controls.Add(this.ux_groupboxAttachmentFileFilters);
            this.Name = "Options";
            this.Text = "Options";
            this.ux_groupboxAttachmentFileFilters.ResumeLayout(false);
            this.ux_groupboxAttachmentFileFilters.PerformLayout();
            this.ux_groupboxFileNameAutomation.ResumeLayout(false);
            this.ux_groupboxFileNameAutomation.PerformLayout();
            this.ux_groupboxTokenMapping.ResumeLayout(false);
            this.ux_groupboxTokenMapping.PerformLayout();
            this.ux_groupboxFixedFieldTokens.ResumeLayout(false);
            this.ux_groupboxFixedFieldTokens.PerformLayout();
            this.ux_groupboxDelimiters.ResumeLayout(false);
            this.ux_groupboxDelimiters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ux_labelIncludeFilter;
        private System.Windows.Forms.Label ux_labelExcludeFilter;
        private System.Windows.Forms.TextBox ux_textboxIncludeFilter;
        private System.Windows.Forms.TextBox ux_textboxExcludeFilter;
        private System.Windows.Forms.GroupBox ux_groupboxAttachmentFileFilters;
        private System.Windows.Forms.Button ux_buttonOK;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.GroupBox ux_groupboxFileNameAutomation;
        private System.Windows.Forms.CheckBox ux_checkboxCaseSensitive;
        private System.Windows.Forms.GroupBox ux_groupboxDelimiters;
        private System.Windows.Forms.CheckBox ux_checkboxDash;
        private System.Windows.Forms.TextBox ux_textboxOther;
        private System.Windows.Forms.CheckBox ux_checkboxUnderscore;
        private System.Windows.Forms.CheckBox ux_checkboxOther;
        private System.Windows.Forms.CheckBox ux_checkboxSpace;
        private System.Windows.Forms.RadioButton ux_radiobuttonFixedFieldParse;
        private System.Windows.Forms.RadioButton ux_radiobuttonDelimitedParse;
        private System.Windows.Forms.RadioButton ux_radiobuttonSmartParse;
        private System.Windows.Forms.GroupBox ux_groupboxFixedFieldTokens;
        private System.Windows.Forms.TextBox ux_textboxFixedToken2;
        private System.Windows.Forms.TextBox ux_textboxFixedToken1;
        private System.Windows.Forms.Label ux_labelFixedToken5;
        private System.Windows.Forms.Label ux_labelFixedToken4;
        private System.Windows.Forms.Label ux_labelFixedToken3;
        private System.Windows.Forms.Label ux_labelFixedToken2;
        private System.Windows.Forms.Label ux_labelFixedToken1;
        private System.Windows.Forms.TextBox ux_textboxFixedToken8;
        private System.Windows.Forms.Label ux_labelFixedToken8;
        private System.Windows.Forms.TextBox ux_textboxFixedToken7;
        private System.Windows.Forms.Label ux_labelFixedToken7;
        private System.Windows.Forms.TextBox ux_textboxFixedToken6;
        private System.Windows.Forms.Label ux_labelFixedToken6;
        private System.Windows.Forms.TextBox ux_textboxFixedToken5;
        private System.Windows.Forms.TextBox ux_textboxFixedToken4;
        private System.Windows.Forms.TextBox ux_textboxFixedToken3;
        private System.Windows.Forms.TextBox ux_textboxFixedToken12;
        private System.Windows.Forms.TextBox ux_textboxFixedToken11;
        private System.Windows.Forms.TextBox ux_textboxFixedToken10;
        private System.Windows.Forms.TextBox ux_textboxFixedToken9;
        private System.Windows.Forms.Label ux_labelFixedToken12;
        private System.Windows.Forms.Label ux_labelFixedToken11;
        private System.Windows.Forms.Label ux_labelFixedToken10;
        private System.Windows.Forms.Label ux_labelFixedToken9;
        private System.Windows.Forms.GroupBox ux_groupboxTokenMapping;
        private System.Windows.Forms.TextBox ux_textboxContent;
        private System.Windows.Forms.Label ux_labelContent;
        private System.Windows.Forms.TextBox ux_textboxDate;
        private System.Windows.Forms.Label ux_labelDate;
        private System.Windows.Forms.TextBox ux_textboxFormType;
        private System.Windows.Forms.Label ux_labelFormType;
        private System.Windows.Forms.TextBox ux_textboxSuffix;
        private System.Windows.Forms.Label ux_labelSuffix;
        private System.Windows.Forms.TextBox ux_textboxNumber;
        private System.Windows.Forms.Label ux_labelNumber;
        private System.Windows.Forms.TextBox ux_textboxPrefix;
        private System.Windows.Forms.Label ux_labelPrefix;
    }
}
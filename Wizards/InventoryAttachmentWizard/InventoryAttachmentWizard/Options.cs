﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InventoryAttachmentWizard
{
    public partial class Options : Form
    {
        string _includeFilter = "";
        string _excludeFilter = "";
        string _parseMethod = "SmartParse";
        bool _caseSensitive = false;
        char[] _delimiters = new char[2] {'_', ' '};
        int[] _fixedFieldTokenLengths = new int[12];
        Dictionary<string, int> _tokenPosition = new Dictionary<string, int>(6);

        public Options()
        {
            InitializeComponent();
        }

        public string IncludeFilter
        {
            get
            {
                return _includeFilter;
            }
            set
            {
                _includeFilter = value;
                ux_textboxIncludeFilter.Text = _includeFilter;
            }
        }
 
        public string ExcludeFilter
        {
            get
            {
                return _excludeFilter;
            }
            set
            {
                _excludeFilter = value;
                ux_textboxExcludeFilter.Text = _excludeFilter;
            }
        }

        public string ParseMethod
        {
            get
            {
                return _parseMethod;
            }
       }

        public bool CaseSensitive
        {
            get
            {
                return _caseSensitive;
            }
        }

        public char[] Delimiters
        {
            get
            {
                return _delimiters;
            }
        }

        public int[] FixedFieldTokenLengths
        {
            get
            {
                return _fixedFieldTokenLengths;
            }
        }

        public Dictionary<string, int> TokenPosition
        {
            get
            {
                return _tokenPosition;
            }
        }

        private void ux_buttonOK_Click(object sender, EventArgs e)
        {
            // Capture the include filename filter...
            _includeFilter = ux_textboxIncludeFilter.Text;

            // Capture the exclude filename filter...
            _excludeFilter = ux_textboxExcludeFilter.Text;

            // Capture the parsing method...
            if (ux_radiobuttonFixedFieldParse.Checked)
            {
                _parseMethod = "FixedFieldParse";
            }
            else if (ux_radiobuttonDelimitedParse.Checked)
            {
                _parseMethod = "DelimitedParse";
            }
            else
            {
                _parseMethod = "SmartParse";
            }

            // Capture case sensitivity...
            _caseSensitive = ux_checkboxCaseSensitive.Checked;

            // Capture the delimiters...
            string delimiters = "";
            if (ux_checkboxUnderscore.Checked) delimiters += "_";
            if (ux_checkboxSpace.Checked) delimiters += " ";
            if (ux_checkboxDash.Checked) delimiters += "-";
            if (ux_checkboxOther.Checked) delimiters += ux_textboxOther.Text;
            _delimiters = delimiters.ToCharArray();

            // Capture the fixed field token lengths...
            int length = 0;
            for(int i=1; i<13; i++)
            {
                // Find the control...
                Control textboxControl = this.Controls["ux_textboxFixedToken"+i.ToString()];
                if(textboxControl != null)
                {
                    // Set the default length to zero...
                    _fixedFieldTokenLengths[i] = 0;
                    // If the control was found try to convert the text in the control to an int...
                    if(int.TryParse(textboxControl.Text, out length)) _fixedFieldTokenLengths[i] = length;
                }
            }

            // Capture the fixed field token lengths...
            int position = 0;
            _tokenPosition.Clear();
            // Prefix...
            if (int.TryParse(ux_textboxPrefix.Text, out position)) _tokenPosition.Add("prefix", position);
            else _tokenPosition.Add("prefix", 0);
            // Number...
            if (int.TryParse(ux_textboxNumber.Text, out position)) _tokenPosition.Add("number", position);
            else _tokenPosition.Add("number", 0);
            // Suffix...
            if (int.TryParse(ux_textboxSuffix.Text, out position)) _tokenPosition.Add("suffix", position);
            else _tokenPosition.Add("suffix", 0);
            // FormType...
            if (int.TryParse(ux_textboxFormType.Text, out position)) _tokenPosition.Add("formtype", position);
            else _tokenPosition.Add("formtype", 0);
            // Date...
            if (int.TryParse(ux_textboxDate.Text, out position)) _tokenPosition.Add("date", position);
            else _tokenPosition.Add("date", 0);
            // Content...
            if (int.TryParse(ux_textboxContent.Text, out position)) _tokenPosition.Add("content", position);
            else _tokenPosition.Add("content", 0);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void ux_radiobuttonSmartParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = false;
            ux_groupboxFixedFieldTokens.Enabled = false;
        }

        private void ux_radiobuttonDelimitedParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = true;
            ux_groupboxFixedFieldTokens.Enabled = false;
        }

        private void ux_radiobuttonFixedFieldParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = false;
            ux_groupboxFixedFieldTokens.Enabled = true;
        }
    }
}

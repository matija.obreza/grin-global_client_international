﻿namespace ViabilityWizard
{
    partial class ViabilityWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViabilityWizard));
            this.ux_buttonMakeNewInventoryViabilityData = new System.Windows.Forms.Button();
            this.ux_buttonGetInventoryViabilityTest = new System.Windows.Forms.Button();
            this.ux_textboxInventoryViabilityID = new System.Windows.Forms.TextBox();
            this.ux_panelInventoryViabilityData = new System.Windows.Forms.Panel();
            this.ux_groupboxViabilitySummary = new System.Windows.Forms.GroupBox();
            this.ux_groupboxViabilityData = new System.Windows.Forms.GroupBox();
            this.ux_datetimepickerChangeViabilityDate = new System.Windows.Forms.DateTimePicker();
            this.ux_buttonChangeDate = new System.Windows.Forms.Button();
            this.ux_groupboxViabilityDate = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonCount8 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount7 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount6 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount5 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount4 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount3 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount2 = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCount1 = new System.Windows.Forms.RadioButton();
            this.ux_labelInventoryViabilityID = new System.Windows.Forms.Label();
            this.ux_groupboxGetViabilityTest = new System.Windows.Forms.GroupBox();
            this.ux_textboxTaxon = new System.Windows.Forms.TextBox();
            this.ux_labelTaxon = new System.Windows.Forms.Label();
            this.ux_textboxViabilityTestedDate = new System.Windows.Forms.TextBox();
            this.ux_labelViabilityTestedDate = new System.Windows.Forms.Label();
            this.ux_textboxPercentViable = new System.Windows.Forms.TextBox();
            this.ux_labelPercentViable = new System.Windows.Forms.Label();
            this.ux_labelInventoryID = new System.Windows.Forms.Label();
            this.ux_textboxInventoryID = new System.Windows.Forms.TextBox();
            this.ux_buttonSaveIVChanges = new System.Windows.Forms.Button();
            this.ux_buttonCancelIVChanges = new System.Windows.Forms.Button();
            this.ux_comboboxViabilityRule = new System.Windows.Forms.ComboBox();
            this.ux_checkboxShowAllRules = new System.Windows.Forms.CheckBox();
            this.ux_labelViabilityRule = new System.Windows.Forms.Label();
            this.ux_textboxTotalSeeds = new System.Windows.Forms.TextBox();
            this.ux_labelTotalSeeds = new System.Windows.Forms.Label();
            this.ux_textboxNumberOfReplicates = new System.Windows.Forms.TextBox();
            this.ux_labelReplicates = new System.Windows.Forms.Label();
            this.ux_buttonPrintLabels = new System.Windows.Forms.Button();
            this.ux_buttonCancelIVDChanges = new System.Windows.Forms.Button();
            this.ux_buttonSaveIVDChanges = new System.Windows.Forms.Button();
            this.ux_groupboxCreateViabilityTest = new System.Windows.Forms.GroupBox();
            this.ux_textboxCreateFromInventory = new System.Windows.Forms.TextBox();
            this.ux_labelCreateFromInventory = new System.Windows.Forms.Label();
            this.ux_buttonCreateFromInventory = new System.Windows.Forms.Button();
            this.ux_textboxCreateFromOrder = new System.Windows.Forms.TextBox();
            this.ux_labelCreateFromOrder = new System.Windows.Forms.Label();
            this.ux_buttonCreateFromOrder = new System.Windows.Forms.Button();
            this.ux_buttonPostFinalResults = new System.Windows.Forms.Button();
            this.ux_groupboxViabilityRule = new System.Windows.Forms.GroupBox();
            this.ux_comboboxCrystalReports = new System.Windows.Forms.ComboBox();
            this.ux_textboxLighting = new System.Windows.Forms.TextBox();
            this.ux_labelLighting = new System.Windows.Forms.Label();
            this.ux_textboxTemperature = new System.Windows.Forms.TextBox();
            this.ux_textboxPrechill = new System.Windows.Forms.TextBox();
            this.ux_textboxMoisture = new System.Windows.Forms.TextBox();
            this.ux_textboxSubstrata = new System.Windows.Forms.TextBox();
            this.ux_labelTemperature = new System.Windows.Forms.Label();
            this.ux_labelPrechill = new System.Windows.Forms.Label();
            this.ux_labelMoisture = new System.Windows.Forms.Label();
            this.ux_labelSubstrata = new System.Windows.Forms.Label();
            this.ux_labelRuleNotes = new System.Windows.Forms.Label();
            this.ux_textboxRuleNotes = new System.Windows.Forms.TextBox();
            this.ux_labelTaxonomyNotes = new System.Windows.Forms.Label();
            this.ux_textboxTaxonomyNotes = new System.Windows.Forms.TextBox();
            this.ux_labelViabilityNotes = new System.Windows.Forms.Label();
            this.ux_textboxViabilityNotes = new System.Windows.Forms.TextBox();
            this.ux_buttonCreateViabilityDataRecords = new System.Windows.Forms.Button();
            this.ux_labelMessageDisplay = new System.Windows.Forms.Label();
            this.ux_buttonCalcDormancyEstimatesNow = new System.Windows.Forms.Button();
            this.ux_panelInventoryViabilityData.SuspendLayout();
            this.ux_groupboxViabilityData.SuspendLayout();
            this.ux_groupboxViabilityDate.SuspendLayout();
            this.ux_groupboxGetViabilityTest.SuspendLayout();
            this.ux_groupboxCreateViabilityTest.SuspendLayout();
            this.ux_groupboxViabilityRule.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_buttonMakeNewInventoryViabilityData
            // 
            this.ux_buttonMakeNewInventoryViabilityData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonMakeNewInventoryViabilityData.Location = new System.Drawing.Point(540, 666);
            this.ux_buttonMakeNewInventoryViabilityData.Name = "ux_buttonMakeNewInventoryViabilityData";
            this.ux_buttonMakeNewInventoryViabilityData.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonMakeNewInventoryViabilityData.TabIndex = 0;
            this.ux_buttonMakeNewInventoryViabilityData.Text = "Make New Tests...";
            this.ux_buttonMakeNewInventoryViabilityData.UseVisualStyleBackColor = true;
            this.ux_buttonMakeNewInventoryViabilityData.Visible = false;
            this.ux_buttonMakeNewInventoryViabilityData.Click += new System.EventHandler(this.ux_buttonMakeNewInventoryViabilityData_Click);
            // 
            // ux_buttonGetInventoryViabilityTest
            // 
            this.ux_buttonGetInventoryViabilityTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonGetInventoryViabilityTest.Location = new System.Drawing.Point(485, 17);
            this.ux_buttonGetInventoryViabilityTest.Name = "ux_buttonGetInventoryViabilityTest";
            this.ux_buttonGetInventoryViabilityTest.Size = new System.Drawing.Size(126, 23);
            this.ux_buttonGetInventoryViabilityTest.TabIndex = 2;
            this.ux_buttonGetInventoryViabilityTest.Text = "Get Test";
            this.ux_buttonGetInventoryViabilityTest.UseVisualStyleBackColor = true;
            this.ux_buttonGetInventoryViabilityTest.Click += new System.EventHandler(this.ux_buttonGetInventoryViabilityTest_Click);
            // 
            // ux_textboxInventoryViabilityID
            // 
            this.ux_textboxInventoryViabilityID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxInventoryViabilityID.Location = new System.Drawing.Point(132, 19);
            this.ux_textboxInventoryViabilityID.Name = "ux_textboxInventoryViabilityID";
            this.ux_textboxInventoryViabilityID.Size = new System.Drawing.Size(347, 20);
            this.ux_textboxInventoryViabilityID.TabIndex = 3;
            this.ux_textboxInventoryViabilityID.TextChanged += new System.EventHandler(this.ux_textboxInventoryViabilityID_TextChanged);
            this.ux_textboxInventoryViabilityID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ux_textboxInventoryViabilityID_KeyPress);
            // 
            // ux_panelInventoryViabilityData
            // 
            this.ux_panelInventoryViabilityData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_panelInventoryViabilityData.Controls.Add(this.ux_groupboxViabilitySummary);
            this.ux_panelInventoryViabilityData.Controls.Add(this.ux_groupboxViabilityData);
            this.ux_panelInventoryViabilityData.Controls.Add(this.ux_groupboxViabilityDate);
            this.ux_panelInventoryViabilityData.Location = new System.Drawing.Point(2, 340);
            this.ux_panelInventoryViabilityData.Name = "ux_panelInventoryViabilityData";
            this.ux_panelInventoryViabilityData.Size = new System.Drawing.Size(1004, 298);
            this.ux_panelInventoryViabilityData.TabIndex = 4;
            // 
            // ux_groupboxViabilitySummary
            // 
            this.ux_groupboxViabilitySummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxViabilitySummary.Location = new System.Drawing.Point(7, 247);
            this.ux_groupboxViabilitySummary.Name = "ux_groupboxViabilitySummary";
            this.ux_groupboxViabilitySummary.Size = new System.Drawing.Size(984, 48);
            this.ux_groupboxViabilitySummary.TabIndex = 2;
            this.ux_groupboxViabilitySummary.TabStop = false;
            this.ux_groupboxViabilitySummary.Text = "Viability Summary";
            // 
            // ux_groupboxViabilityData
            // 
            this.ux_groupboxViabilityData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxViabilityData.Controls.Add(this.ux_buttonCalcDormancyEstimatesNow);
            this.ux_groupboxViabilityData.Controls.Add(this.ux_datetimepickerChangeViabilityDate);
            this.ux_groupboxViabilityData.Controls.Add(this.ux_buttonChangeDate);
            this.ux_groupboxViabilityData.Location = new System.Drawing.Point(7, 54);
            this.ux_groupboxViabilityData.Name = "ux_groupboxViabilityData";
            this.ux_groupboxViabilityData.Size = new System.Drawing.Size(984, 194);
            this.ux_groupboxViabilityData.TabIndex = 1;
            this.ux_groupboxViabilityData.TabStop = false;
            this.ux_groupboxViabilityData.Text = "Viability Count Data";
            // 
            // ux_datetimepickerChangeViabilityDate
            // 
            this.ux_datetimepickerChangeViabilityDate.Location = new System.Drawing.Point(105, 19);
            this.ux_datetimepickerChangeViabilityDate.Name = "ux_datetimepickerChangeViabilityDate";
            this.ux_datetimepickerChangeViabilityDate.Size = new System.Drawing.Size(200, 20);
            this.ux_datetimepickerChangeViabilityDate.TabIndex = 10;
            this.ux_datetimepickerChangeViabilityDate.Visible = false;
            // 
            // ux_buttonChangeDate
            // 
            this.ux_buttonChangeDate.Location = new System.Drawing.Point(6, 19);
            this.ux_buttonChangeDate.Name = "ux_buttonChangeDate";
            this.ux_buttonChangeDate.Size = new System.Drawing.Size(92, 23);
            this.ux_buttonChangeDate.TabIndex = 9;
            this.ux_buttonChangeDate.Text = "Change Date";
            this.ux_buttonChangeDate.UseVisualStyleBackColor = true;
            this.ux_buttonChangeDate.Click += new System.EventHandler(this.ux_buttonChangeDate_Click);
            // 
            // ux_groupboxViabilityDate
            // 
            this.ux_groupboxViabilityDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount8);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount7);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount6);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount5);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount4);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount3);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount2);
            this.ux_groupboxViabilityDate.Controls.Add(this.ux_radiobuttonCount1);
            this.ux_groupboxViabilityDate.Location = new System.Drawing.Point(7, 4);
            this.ux_groupboxViabilityDate.Name = "ux_groupboxViabilityDate";
            this.ux_groupboxViabilityDate.Size = new System.Drawing.Size(984, 51);
            this.ux_groupboxViabilityDate.TabIndex = 0;
            this.ux_groupboxViabilityDate.TabStop = false;
            this.ux_groupboxViabilityDate.Text = "Viability Date";
            // 
            // ux_radiobuttonCount8
            // 
            this.ux_radiobuttonCount8.AutoSize = true;
            this.ux_radiobuttonCount8.Location = new System.Drawing.Point(694, 20);
            this.ux_radiobuttonCount8.Name = "ux_radiobuttonCount8";
            this.ux_radiobuttonCount8.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount8.TabIndex = 7;
            this.ux_radiobuttonCount8.TabStop = true;
            this.ux_radiobuttonCount8.Tag = "8";
            this.ux_radiobuttonCount8.Text = "Count 8";
            this.ux_radiobuttonCount8.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount8.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount7
            // 
            this.ux_radiobuttonCount7.AutoSize = true;
            this.ux_radiobuttonCount7.Location = new System.Drawing.Point(597, 20);
            this.ux_radiobuttonCount7.Name = "ux_radiobuttonCount7";
            this.ux_radiobuttonCount7.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount7.TabIndex = 6;
            this.ux_radiobuttonCount7.TabStop = true;
            this.ux_radiobuttonCount7.Tag = "7";
            this.ux_radiobuttonCount7.Text = "Count 7";
            this.ux_radiobuttonCount7.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount7.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount6
            // 
            this.ux_radiobuttonCount6.AutoSize = true;
            this.ux_radiobuttonCount6.Location = new System.Drawing.Point(500, 20);
            this.ux_radiobuttonCount6.Name = "ux_radiobuttonCount6";
            this.ux_radiobuttonCount6.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount6.TabIndex = 5;
            this.ux_radiobuttonCount6.TabStop = true;
            this.ux_radiobuttonCount6.Tag = "6";
            this.ux_radiobuttonCount6.Text = "Count 6";
            this.ux_radiobuttonCount6.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount6.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount5
            // 
            this.ux_radiobuttonCount5.AutoSize = true;
            this.ux_radiobuttonCount5.Location = new System.Drawing.Point(403, 20);
            this.ux_radiobuttonCount5.Name = "ux_radiobuttonCount5";
            this.ux_radiobuttonCount5.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount5.TabIndex = 4;
            this.ux_radiobuttonCount5.TabStop = true;
            this.ux_radiobuttonCount5.Tag = "5";
            this.ux_radiobuttonCount5.Text = "Count 5";
            this.ux_radiobuttonCount5.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount5.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount4
            // 
            this.ux_radiobuttonCount4.AutoSize = true;
            this.ux_radiobuttonCount4.Location = new System.Drawing.Point(306, 20);
            this.ux_radiobuttonCount4.Name = "ux_radiobuttonCount4";
            this.ux_radiobuttonCount4.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount4.TabIndex = 3;
            this.ux_radiobuttonCount4.TabStop = true;
            this.ux_radiobuttonCount4.Tag = "4";
            this.ux_radiobuttonCount4.Text = "Count 4";
            this.ux_radiobuttonCount4.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount4.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount3
            // 
            this.ux_radiobuttonCount3.AutoSize = true;
            this.ux_radiobuttonCount3.Location = new System.Drawing.Point(209, 20);
            this.ux_radiobuttonCount3.Name = "ux_radiobuttonCount3";
            this.ux_radiobuttonCount3.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount3.TabIndex = 2;
            this.ux_radiobuttonCount3.TabStop = true;
            this.ux_radiobuttonCount3.Tag = "3";
            this.ux_radiobuttonCount3.Text = "Count 3";
            this.ux_radiobuttonCount3.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount3.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount2
            // 
            this.ux_radiobuttonCount2.AutoSize = true;
            this.ux_radiobuttonCount2.Location = new System.Drawing.Point(112, 20);
            this.ux_radiobuttonCount2.Name = "ux_radiobuttonCount2";
            this.ux_radiobuttonCount2.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount2.TabIndex = 1;
            this.ux_radiobuttonCount2.TabStop = true;
            this.ux_radiobuttonCount2.Tag = "2";
            this.ux_radiobuttonCount2.Text = "Count 2";
            this.ux_radiobuttonCount2.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount2.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_radiobuttonCount1
            // 
            this.ux_radiobuttonCount1.AutoSize = true;
            this.ux_radiobuttonCount1.Location = new System.Drawing.Point(15, 20);
            this.ux_radiobuttonCount1.Name = "ux_radiobuttonCount1";
            this.ux_radiobuttonCount1.Size = new System.Drawing.Size(62, 17);
            this.ux_radiobuttonCount1.TabIndex = 0;
            this.ux_radiobuttonCount1.TabStop = true;
            this.ux_radiobuttonCount1.Tag = "1";
            this.ux_radiobuttonCount1.Text = "Count 1";
            this.ux_radiobuttonCount1.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCount1.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCount_CheckedChanged);
            // 
            // ux_labelInventoryViabilityID
            // 
            this.ux_labelInventoryViabilityID.Location = new System.Drawing.Point(6, 22);
            this.ux_labelInventoryViabilityID.Name = "ux_labelInventoryViabilityID";
            this.ux_labelInventoryViabilityID.Size = new System.Drawing.Size(120, 13);
            this.ux_labelInventoryViabilityID.TabIndex = 5;
            this.ux_labelInventoryViabilityID.Text = "Inventory Viability ID:";
            this.ux_labelInventoryViabilityID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_groupboxGetViabilityTest
            // 
            this.ux_groupboxGetViabilityTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_textboxTaxon);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_labelTaxon);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_textboxViabilityTestedDate);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_labelViabilityTestedDate);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_textboxPercentViable);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_labelPercentViable);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_labelInventoryID);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_textboxInventoryID);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_labelInventoryViabilityID);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_textboxInventoryViabilityID);
            this.ux_groupboxGetViabilityTest.Controls.Add(this.ux_buttonGetInventoryViabilityTest);
            this.ux_groupboxGetViabilityTest.Location = new System.Drawing.Point(9, 12);
            this.ux_groupboxGetViabilityTest.Name = "ux_groupboxGetViabilityTest";
            this.ux_groupboxGetViabilityTest.Size = new System.Drawing.Size(622, 99);
            this.ux_groupboxGetViabilityTest.TabIndex = 5;
            this.ux_groupboxGetViabilityTest.TabStop = false;
            this.ux_groupboxGetViabilityTest.Text = "Get Existing Viability Test";
            // 
            // ux_textboxTaxon
            // 
            this.ux_textboxTaxon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxTaxon.Location = new System.Drawing.Point(447, 72);
            this.ux_textboxTaxon.Name = "ux_textboxTaxon";
            this.ux_textboxTaxon.ReadOnly = true;
            this.ux_textboxTaxon.Size = new System.Drawing.Size(164, 20);
            this.ux_textboxTaxon.TabIndex = 32;
            this.ux_textboxTaxon.Tag = "viability_tested_date";
            // 
            // ux_labelTaxon
            // 
            this.ux_labelTaxon.Location = new System.Drawing.Point(393, 75);
            this.ux_labelTaxon.Name = "ux_labelTaxon";
            this.ux_labelTaxon.Size = new System.Drawing.Size(48, 13);
            this.ux_labelTaxon.TabIndex = 31;
            this.ux_labelTaxon.Tag = "viability_tested_date";
            this.ux_labelTaxon.Text = "Taxon:";
            this.ux_labelTaxon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_textboxViabilityTestedDate
            // 
            this.ux_textboxViabilityTestedDate.Location = new System.Drawing.Point(296, 72);
            this.ux_textboxViabilityTestedDate.Name = "ux_textboxViabilityTestedDate";
            this.ux_textboxViabilityTestedDate.ReadOnly = true;
            this.ux_textboxViabilityTestedDate.Size = new System.Drawing.Size(91, 20);
            this.ux_textboxViabilityTestedDate.TabIndex = 30;
            this.ux_textboxViabilityTestedDate.Tag = "viability_tested_date";
            // 
            // ux_labelViabilityTestedDate
            // 
            this.ux_labelViabilityTestedDate.Location = new System.Drawing.Point(205, 75);
            this.ux_labelViabilityTestedDate.Name = "ux_labelViabilityTestedDate";
            this.ux_labelViabilityTestedDate.Size = new System.Drawing.Size(85, 13);
            this.ux_labelViabilityTestedDate.TabIndex = 29;
            this.ux_labelViabilityTestedDate.Tag = "viability_tested_date";
            this.ux_labelViabilityTestedDate.Text = "Last Test Date:";
            this.ux_labelViabilityTestedDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_textboxPercentViable
            // 
            this.ux_textboxPercentViable.Location = new System.Drawing.Point(132, 72);
            this.ux_textboxPercentViable.Name = "ux_textboxPercentViable";
            this.ux_textboxPercentViable.ReadOnly = true;
            this.ux_textboxPercentViable.Size = new System.Drawing.Size(67, 20);
            this.ux_textboxPercentViable.TabIndex = 28;
            this.ux_textboxPercentViable.Tag = "percent_viable";
            // 
            // ux_labelPercentViable
            // 
            this.ux_labelPercentViable.Location = new System.Drawing.Point(6, 75);
            this.ux_labelPercentViable.Name = "ux_labelPercentViable";
            this.ux_labelPercentViable.Size = new System.Drawing.Size(120, 13);
            this.ux_labelPercentViable.TabIndex = 27;
            this.ux_labelPercentViable.Tag = "percent_viable";
            this.ux_labelPercentViable.Text = "Last Percent Viable:";
            this.ux_labelPercentViable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_labelInventoryID
            // 
            this.ux_labelInventoryID.Location = new System.Drawing.Point(6, 49);
            this.ux_labelInventoryID.Name = "ux_labelInventoryID";
            this.ux_labelInventoryID.Size = new System.Drawing.Size(120, 13);
            this.ux_labelInventoryID.TabIndex = 8;
            this.ux_labelInventoryID.Tag = "inventory_id";
            this.ux_labelInventoryID.Text = "Inventory Number:";
            this.ux_labelInventoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_textboxInventoryID
            // 
            this.ux_textboxInventoryID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxInventoryID.Location = new System.Drawing.Point(132, 46);
            this.ux_textboxInventoryID.Name = "ux_textboxInventoryID";
            this.ux_textboxInventoryID.ReadOnly = true;
            this.ux_textboxInventoryID.Size = new System.Drawing.Size(479, 20);
            this.ux_textboxInventoryID.TabIndex = 7;
            this.ux_textboxInventoryID.Tag = "inventory_id";
            // 
            // ux_buttonSaveIVChanges
            // 
            this.ux_buttonSaveIVChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveIVChanges.Location = new System.Drawing.Point(845, 159);
            this.ux_buttonSaveIVChanges.Name = "ux_buttonSaveIVChanges";
            this.ux_buttonSaveIVChanges.Size = new System.Drawing.Size(65, 23);
            this.ux_buttonSaveIVChanges.TabIndex = 32;
            this.ux_buttonSaveIVChanges.Text = "Save";
            this.ux_buttonSaveIVChanges.UseVisualStyleBackColor = true;
            this.ux_buttonSaveIVChanges.Visible = false;
            this.ux_buttonSaveIVChanges.Click += new System.EventHandler(this.ux_buttonSaveIVChanges_Click);
            // 
            // ux_buttonCancelIVChanges
            // 
            this.ux_buttonCancelIVChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancelIVChanges.Location = new System.Drawing.Point(916, 159);
            this.ux_buttonCancelIVChanges.Name = "ux_buttonCancelIVChanges";
            this.ux_buttonCancelIVChanges.Size = new System.Drawing.Size(65, 23);
            this.ux_buttonCancelIVChanges.TabIndex = 31;
            this.ux_buttonCancelIVChanges.Text = "Cancel";
            this.ux_buttonCancelIVChanges.UseVisualStyleBackColor = true;
            this.ux_buttonCancelIVChanges.Visible = false;
            this.ux_buttonCancelIVChanges.Click += new System.EventHandler(this.ux_buttonCancelIVChanges_Click);
            // 
            // ux_comboboxViabilityRule
            // 
            this.ux_comboboxViabilityRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxViabilityRule.FormattingEnabled = true;
            this.ux_comboboxViabilityRule.Location = new System.Drawing.Point(684, 135);
            this.ux_comboboxViabilityRule.Name = "ux_comboboxViabilityRule";
            this.ux_comboboxViabilityRule.Size = new System.Drawing.Size(297, 21);
            this.ux_comboboxViabilityRule.TabIndex = 26;
            this.ux_comboboxViabilityRule.Tag = "name";
            this.ux_comboboxViabilityRule.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxViabilityRule_SelectedIndexChanged);
            // 
            // ux_checkboxShowAllRules
            // 
            this.ux_checkboxShowAllRules.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkboxShowAllRules.AutoSize = true;
            this.ux_checkboxShowAllRules.Location = new System.Drawing.Point(886, 117);
            this.ux_checkboxShowAllRules.Name = "ux_checkboxShowAllRules";
            this.ux_checkboxShowAllRules.Size = new System.Drawing.Size(97, 17);
            this.ux_checkboxShowAllRules.TabIndex = 25;
            this.ux_checkboxShowAllRules.Text = "Show All Rules";
            this.ux_checkboxShowAllRules.UseVisualStyleBackColor = true;
            this.ux_checkboxShowAllRules.CheckedChanged += new System.EventHandler(this.ux_checkboxShowAllRules_CheckedChanged);
            // 
            // ux_labelViabilityRule
            // 
            this.ux_labelViabilityRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelViabilityRule.AutoSize = true;
            this.ux_labelViabilityRule.Location = new System.Drawing.Point(681, 118);
            this.ux_labelViabilityRule.Name = "ux_labelViabilityRule";
            this.ux_labelViabilityRule.Size = new System.Drawing.Size(70, 13);
            this.ux_labelViabilityRule.TabIndex = 20;
            this.ux_labelViabilityRule.Tag = "name";
            this.ux_labelViabilityRule.Text = "Viability Rule:";
            // 
            // ux_textboxTotalSeeds
            // 
            this.ux_textboxTotalSeeds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxTotalSeeds.Location = new System.Drawing.Point(611, 135);
            this.ux_textboxTotalSeeds.Name = "ux_textboxTotalSeeds";
            this.ux_textboxTotalSeeds.Size = new System.Drawing.Size(67, 20);
            this.ux_textboxTotalSeeds.TabIndex = 24;
            this.ux_textboxTotalSeeds.Tag = "seeds_per_replicate";
            this.ux_textboxTotalSeeds.TextChanged += new System.EventHandler(this.ux_textboxTotalSeeds_TextChanged);
            // 
            // ux_labelTotalSeeds
            // 
            this.ux_labelTotalSeeds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelTotalSeeds.AutoSize = true;
            this.ux_labelTotalSeeds.Location = new System.Drawing.Point(608, 118);
            this.ux_labelTotalSeeds.Name = "ux_labelTotalSeeds";
            this.ux_labelTotalSeeds.Size = new System.Drawing.Size(67, 13);
            this.ux_labelTotalSeeds.TabIndex = 23;
            this.ux_labelTotalSeeds.Tag = "seeds_per_replicate";
            this.ux_labelTotalSeeds.Text = "Total Seeds:";
            // 
            // ux_textboxNumberOfReplicates
            // 
            this.ux_textboxNumberOfReplicates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxNumberOfReplicates.Location = new System.Drawing.Point(538, 135);
            this.ux_textboxNumberOfReplicates.Name = "ux_textboxNumberOfReplicates";
            this.ux_textboxNumberOfReplicates.Size = new System.Drawing.Size(67, 20);
            this.ux_textboxNumberOfReplicates.TabIndex = 22;
            this.ux_textboxNumberOfReplicates.Tag = "replicates";
            this.ux_textboxNumberOfReplicates.TextChanged += new System.EventHandler(this.ux_textboxNumberOfReplicates_TextChanged);
            // 
            // ux_labelReplicates
            // 
            this.ux_labelReplicates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelReplicates.AutoSize = true;
            this.ux_labelReplicates.Location = new System.Drawing.Point(535, 118);
            this.ux_labelReplicates.Name = "ux_labelReplicates";
            this.ux_labelReplicates.Size = new System.Drawing.Size(60, 13);
            this.ux_labelReplicates.TabIndex = 21;
            this.ux_labelReplicates.Tag = "replicates";
            this.ux_labelReplicates.Text = "Replicates:";
            // 
            // ux_buttonPrintLabels
            // 
            this.ux_buttonPrintLabels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonPrintLabels.Location = new System.Drawing.Point(537, 192);
            this.ux_buttonPrintLabels.Name = "ux_buttonPrintLabels";
            this.ux_buttonPrintLabels.Size = new System.Drawing.Size(92, 23);
            this.ux_buttonPrintLabels.TabIndex = 6;
            this.ux_buttonPrintLabels.Text = "Print Labels";
            this.ux_buttonPrintLabels.UseVisualStyleBackColor = true;
            this.ux_buttonPrintLabels.Click += new System.EventHandler(this.ux_buttonPrintLabels_Click);
            // 
            // ux_buttonCancelIVDChanges
            // 
            this.ux_buttonCancelIVDChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancelIVDChanges.Location = new System.Drawing.Point(888, 654);
            this.ux_buttonCancelIVDChanges.Name = "ux_buttonCancelIVDChanges";
            this.ux_buttonCancelIVDChanges.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonCancelIVDChanges.TabIndex = 6;
            this.ux_buttonCancelIVDChanges.Text = "Cancel";
            this.ux_buttonCancelIVDChanges.UseVisualStyleBackColor = true;
            this.ux_buttonCancelIVDChanges.Click += new System.EventHandler(this.ux_buttonCancelIVDChanges_Click);
            // 
            // ux_buttonSaveIVDChanges
            // 
            this.ux_buttonSaveIVDChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveIVDChanges.Location = new System.Drawing.Point(772, 654);
            this.ux_buttonSaveIVDChanges.Name = "ux_buttonSaveIVDChanges";
            this.ux_buttonSaveIVDChanges.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonSaveIVDChanges.TabIndex = 7;
            this.ux_buttonSaveIVDChanges.Text = "Save";
            this.ux_buttonSaveIVDChanges.UseVisualStyleBackColor = true;
            this.ux_buttonSaveIVDChanges.Click += new System.EventHandler(this.ux_buttonSaveIVDChanges_Click);
            // 
            // ux_groupboxCreateViabilityTest
            // 
            this.ux_groupboxCreateViabilityTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_textboxCreateFromInventory);
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_labelCreateFromInventory);
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_buttonCreateFromInventory);
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_textboxCreateFromOrder);
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_labelCreateFromOrder);
            this.ux_groupboxCreateViabilityTest.Controls.Add(this.ux_buttonCreateFromOrder);
            this.ux_groupboxCreateViabilityTest.Location = new System.Drawing.Point(637, 13);
            this.ux_groupboxCreateViabilityTest.Name = "ux_groupboxCreateViabilityTest";
            this.ux_groupboxCreateViabilityTest.Size = new System.Drawing.Size(359, 98);
            this.ux_groupboxCreateViabilityTest.TabIndex = 8;
            this.ux_groupboxCreateViabilityTest.TabStop = false;
            this.ux_groupboxCreateViabilityTest.Text = "Create New Viability Test";
            // 
            // ux_textboxCreateFromInventory
            // 
            this.ux_textboxCreateFromInventory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCreateFromInventory.Location = new System.Drawing.Point(97, 48);
            this.ux_textboxCreateFromInventory.Name = "ux_textboxCreateFromInventory";
            this.ux_textboxCreateFromInventory.Size = new System.Drawing.Size(199, 20);
            this.ux_textboxCreateFromInventory.TabIndex = 8;
            // 
            // ux_labelCreateFromInventory
            // 
            this.ux_labelCreateFromInventory.Location = new System.Drawing.Point(6, 51);
            this.ux_labelCreateFromInventory.Name = "ux_labelCreateFromInventory";
            this.ux_labelCreateFromInventory.Size = new System.Drawing.Size(85, 13);
            this.ux_labelCreateFromInventory.TabIndex = 7;
            this.ux_labelCreateFromInventory.Text = "From Inventory:";
            this.ux_labelCreateFromInventory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_buttonCreateFromInventory
            // 
            this.ux_buttonCreateFromInventory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCreateFromInventory.Location = new System.Drawing.Point(302, 46);
            this.ux_buttonCreateFromInventory.Name = "ux_buttonCreateFromInventory";
            this.ux_buttonCreateFromInventory.Size = new System.Drawing.Size(51, 23);
            this.ux_buttonCreateFromInventory.TabIndex = 9;
            this.ux_buttonCreateFromInventory.Text = "Create";
            this.ux_buttonCreateFromInventory.UseVisualStyleBackColor = true;
            this.ux_buttonCreateFromInventory.Click += new System.EventHandler(this.ux_buttonCreateFromInventory_Click);
            // 
            // ux_textboxCreateFromOrder
            // 
            this.ux_textboxCreateFromOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCreateFromOrder.Location = new System.Drawing.Point(97, 18);
            this.ux_textboxCreateFromOrder.Name = "ux_textboxCreateFromOrder";
            this.ux_textboxCreateFromOrder.Size = new System.Drawing.Size(199, 20);
            this.ux_textboxCreateFromOrder.TabIndex = 5;
            // 
            // ux_labelCreateFromOrder
            // 
            this.ux_labelCreateFromOrder.Location = new System.Drawing.Point(6, 21);
            this.ux_labelCreateFromOrder.Name = "ux_labelCreateFromOrder";
            this.ux_labelCreateFromOrder.Size = new System.Drawing.Size(85, 13);
            this.ux_labelCreateFromOrder.TabIndex = 4;
            this.ux_labelCreateFromOrder.Text = "From Order:";
            this.ux_labelCreateFromOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_buttonCreateFromOrder
            // 
            this.ux_buttonCreateFromOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCreateFromOrder.Location = new System.Drawing.Point(302, 16);
            this.ux_buttonCreateFromOrder.Name = "ux_buttonCreateFromOrder";
            this.ux_buttonCreateFromOrder.Size = new System.Drawing.Size(51, 23);
            this.ux_buttonCreateFromOrder.TabIndex = 6;
            this.ux_buttonCreateFromOrder.Text = "Create";
            this.ux_buttonCreateFromOrder.UseVisualStyleBackColor = true;
            this.ux_buttonCreateFromOrder.Click += new System.EventHandler(this.ux_buttonCreateFromOrder_Click);
            // 
            // ux_buttonPostFinalResults
            // 
            this.ux_buttonPostFinalResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonPostFinalResults.Location = new System.Drawing.Point(656, 654);
            this.ux_buttonPostFinalResults.Name = "ux_buttonPostFinalResults";
            this.ux_buttonPostFinalResults.Size = new System.Drawing.Size(110, 23);
            this.ux_buttonPostFinalResults.TabIndex = 10;
            this.ux_buttonPostFinalResults.Text = "Post Final Results";
            this.ux_buttonPostFinalResults.UseVisualStyleBackColor = true;
            this.ux_buttonPostFinalResults.Click += new System.EventHandler(this.ux_buttonPostFinalResults_Click);
            // 
            // ux_groupboxViabilityRule
            // 
            this.ux_groupboxViabilityRule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_comboboxCrystalReports);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxLighting);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelLighting);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxTemperature);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxPrechill);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxMoisture);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxSubstrata);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelTemperature);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelPrechill);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelMoisture);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelSubstrata);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelRuleNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxRuleNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelTaxonomyNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxTaxonomyNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelViabilityNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxViabilityNotes);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_buttonCreateViabilityDataRecords);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_buttonSaveIVChanges);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_comboboxViabilityRule);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelViabilityRule);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_buttonCancelIVChanges);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_checkboxShowAllRules);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxTotalSeeds);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_buttonPrintLabels);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelTotalSeeds);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_labelReplicates);
            this.ux_groupboxViabilityRule.Controls.Add(this.ux_textboxNumberOfReplicates);
            this.ux_groupboxViabilityRule.Location = new System.Drawing.Point(9, 113);
            this.ux_groupboxViabilityRule.Name = "ux_groupboxViabilityRule";
            this.ux_groupboxViabilityRule.Size = new System.Drawing.Size(989, 221);
            this.ux_groupboxViabilityRule.TabIndex = 10;
            this.ux_groupboxViabilityRule.TabStop = false;
            this.ux_groupboxViabilityRule.Text = "Viability Test Details";
            // 
            // ux_comboboxCrystalReports
            // 
            this.ux_comboboxCrystalReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxCrystalReports.FormattingEnabled = true;
            this.ux_comboboxCrystalReports.Location = new System.Drawing.Point(638, 193);
            this.ux_comboboxCrystalReports.Name = "ux_comboboxCrystalReports";
            this.ux_comboboxCrystalReports.Size = new System.Drawing.Size(343, 21);
            this.ux_comboboxCrystalReports.TabIndex = 51;
            // 
            // ux_textboxLighting
            // 
            this.ux_textboxLighting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxLighting.Location = new System.Drawing.Point(822, 94);
            this.ux_textboxLighting.Name = "ux_textboxLighting";
            this.ux_textboxLighting.ReadOnly = true;
            this.ux_textboxLighting.Size = new System.Drawing.Size(159, 20);
            this.ux_textboxLighting.TabIndex = 50;
            this.ux_textboxLighting.Tag = "replicates";
            // 
            // ux_labelLighting
            // 
            this.ux_labelLighting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelLighting.AutoSize = true;
            this.ux_labelLighting.Location = new System.Drawing.Point(765, 97);
            this.ux_labelLighting.Name = "ux_labelLighting";
            this.ux_labelLighting.Size = new System.Drawing.Size(44, 13);
            this.ux_labelLighting.TabIndex = 49;
            this.ux_labelLighting.Text = "Lighting";
            // 
            // ux_textboxTemperature
            // 
            this.ux_textboxTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxTemperature.Location = new System.Drawing.Point(592, 94);
            this.ux_textboxTemperature.Name = "ux_textboxTemperature";
            this.ux_textboxTemperature.ReadOnly = true;
            this.ux_textboxTemperature.Size = new System.Drawing.Size(159, 20);
            this.ux_textboxTemperature.TabIndex = 48;
            this.ux_textboxTemperature.Tag = "replicates";
            // 
            // ux_textboxPrechill
            // 
            this.ux_textboxPrechill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxPrechill.Location = new System.Drawing.Point(592, 68);
            this.ux_textboxPrechill.Name = "ux_textboxPrechill";
            this.ux_textboxPrechill.ReadOnly = true;
            this.ux_textboxPrechill.Size = new System.Drawing.Size(389, 20);
            this.ux_textboxPrechill.TabIndex = 47;
            this.ux_textboxPrechill.Tag = "replicates";
            // 
            // ux_textboxMoisture
            // 
            this.ux_textboxMoisture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxMoisture.Location = new System.Drawing.Point(592, 42);
            this.ux_textboxMoisture.Name = "ux_textboxMoisture";
            this.ux_textboxMoisture.ReadOnly = true;
            this.ux_textboxMoisture.Size = new System.Drawing.Size(389, 20);
            this.ux_textboxMoisture.TabIndex = 46;
            this.ux_textboxMoisture.Tag = "replicates";
            // 
            // ux_textboxSubstrata
            // 
            this.ux_textboxSubstrata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxSubstrata.Location = new System.Drawing.Point(592, 17);
            this.ux_textboxSubstrata.Name = "ux_textboxSubstrata";
            this.ux_textboxSubstrata.ReadOnly = true;
            this.ux_textboxSubstrata.Size = new System.Drawing.Size(389, 20);
            this.ux_textboxSubstrata.TabIndex = 45;
            this.ux_textboxSubstrata.Tag = "replicates";
            // 
            // ux_labelTemperature
            // 
            this.ux_labelTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelTemperature.AutoSize = true;
            this.ux_labelTemperature.Location = new System.Drawing.Point(535, 97);
            this.ux_labelTemperature.Name = "ux_labelTemperature";
            this.ux_labelTemperature.Size = new System.Drawing.Size(34, 13);
            this.ux_labelTemperature.TabIndex = 43;
            this.ux_labelTemperature.Text = "Temp";
            // 
            // ux_labelPrechill
            // 
            this.ux_labelPrechill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelPrechill.AutoSize = true;
            this.ux_labelPrechill.Location = new System.Drawing.Point(534, 71);
            this.ux_labelPrechill.Name = "ux_labelPrechill";
            this.ux_labelPrechill.Size = new System.Drawing.Size(41, 13);
            this.ux_labelPrechill.TabIndex = 42;
            this.ux_labelPrechill.Text = "Prechill";
            // 
            // ux_labelMoisture
            // 
            this.ux_labelMoisture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelMoisture.AutoSize = true;
            this.ux_labelMoisture.Location = new System.Drawing.Point(535, 45);
            this.ux_labelMoisture.Name = "ux_labelMoisture";
            this.ux_labelMoisture.Size = new System.Drawing.Size(47, 13);
            this.ux_labelMoisture.TabIndex = 41;
            this.ux_labelMoisture.Text = "Moisture";
            // 
            // ux_labelSubstrata
            // 
            this.ux_labelSubstrata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelSubstrata.AutoSize = true;
            this.ux_labelSubstrata.Location = new System.Drawing.Point(535, 20);
            this.ux_labelSubstrata.Name = "ux_labelSubstrata";
            this.ux_labelSubstrata.Size = new System.Drawing.Size(52, 13);
            this.ux_labelSubstrata.TabIndex = 40;
            this.ux_labelSubstrata.Text = "Substrata";
            // 
            // ux_labelRuleNotes
            // 
            this.ux_labelRuleNotes.AutoSize = true;
            this.ux_labelRuleNotes.Location = new System.Drawing.Point(5, 12);
            this.ux_labelRuleNotes.Name = "ux_labelRuleNotes";
            this.ux_labelRuleNotes.Size = new System.Drawing.Size(63, 13);
            this.ux_labelRuleNotes.TabIndex = 39;
            this.ux_labelRuleNotes.Tag = "note";
            this.ux_labelRuleNotes.Text = "Rule Notes:";
            // 
            // ux_textboxRuleNotes
            // 
            this.ux_textboxRuleNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxRuleNotes.Location = new System.Drawing.Point(6, 26);
            this.ux_textboxRuleNotes.Multiline = true;
            this.ux_textboxRuleNotes.Name = "ux_textboxRuleNotes";
            this.ux_textboxRuleNotes.ReadOnly = true;
            this.ux_textboxRuleNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxRuleNotes.Size = new System.Drawing.Size(522, 53);
            this.ux_textboxRuleNotes.TabIndex = 38;
            this.ux_textboxRuleNotes.Text = resources.GetString("ux_textboxRuleNotes.Text");
            // 
            // ux_labelTaxonomyNotes
            // 
            this.ux_labelTaxonomyNotes.AutoSize = true;
            this.ux_labelTaxonomyNotes.Location = new System.Drawing.Point(3, 77);
            this.ux_labelTaxonomyNotes.Name = "ux_labelTaxonomyNotes";
            this.ux_labelTaxonomyNotes.Size = new System.Drawing.Size(90, 13);
            this.ux_labelTaxonomyNotes.TabIndex = 37;
            this.ux_labelTaxonomyNotes.Tag = "note";
            this.ux_labelTaxonomyNotes.Text = "Taxonomy Notes:";
            // 
            // ux_textboxTaxonomyNotes
            // 
            this.ux_textboxTaxonomyNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxTaxonomyNotes.Location = new System.Drawing.Point(3, 93);
            this.ux_textboxTaxonomyNotes.Multiline = true;
            this.ux_textboxTaxonomyNotes.Name = "ux_textboxTaxonomyNotes";
            this.ux_textboxTaxonomyNotes.ReadOnly = true;
            this.ux_textboxTaxonomyNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxTaxonomyNotes.Size = new System.Drawing.Size(522, 53);
            this.ux_textboxTaxonomyNotes.TabIndex = 36;
            this.ux_textboxTaxonomyNotes.Text = resources.GetString("ux_textboxTaxonomyNotes.Text");
            // 
            // ux_labelViabilityNotes
            // 
            this.ux_labelViabilityNotes.AutoSize = true;
            this.ux_labelViabilityNotes.Location = new System.Drawing.Point(3, 148);
            this.ux_labelViabilityNotes.Name = "ux_labelViabilityNotes";
            this.ux_labelViabilityNotes.Size = new System.Drawing.Size(100, 13);
            this.ux_labelViabilityNotes.TabIndex = 35;
            this.ux_labelViabilityNotes.Tag = "note";
            this.ux_labelViabilityNotes.Text = "Viability Test Notes:";
            // 
            // ux_textboxViabilityNotes
            // 
            this.ux_textboxViabilityNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxViabilityNotes.Location = new System.Drawing.Point(6, 162);
            this.ux_textboxViabilityNotes.Multiline = true;
            this.ux_textboxViabilityNotes.Name = "ux_textboxViabilityNotes";
            this.ux_textboxViabilityNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxViabilityNotes.Size = new System.Drawing.Size(522, 53);
            this.ux_textboxViabilityNotes.TabIndex = 34;
            this.ux_textboxViabilityNotes.Text = resources.GetString("ux_textboxViabilityNotes.Text");
            this.ux_textboxViabilityNotes.TextChanged += new System.EventHandler(this.ux_textboxViabilityNotes_TextChanged);
            // 
            // ux_buttonCreateViabilityDataRecords
            // 
            this.ux_buttonCreateViabilityDataRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCreateViabilityDataRecords.Location = new System.Drawing.Point(638, 160);
            this.ux_buttonCreateViabilityDataRecords.Name = "ux_buttonCreateViabilityDataRecords";
            this.ux_buttonCreateViabilityDataRecords.Size = new System.Drawing.Size(201, 23);
            this.ux_buttonCreateViabilityDataRecords.TabIndex = 33;
            this.ux_buttonCreateViabilityDataRecords.Text = "Initialize Count Records Now";
            this.ux_buttonCreateViabilityDataRecords.UseVisualStyleBackColor = true;
            this.ux_buttonCreateViabilityDataRecords.Click += new System.EventHandler(this.ux_buttonCreateViabilityDataRecords_Click);
            // 
            // ux_labelMessageDisplay
            // 
            this.ux_labelMessageDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelMessageDisplay.AutoSize = true;
            this.ux_labelMessageDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelMessageDisplay.Location = new System.Drawing.Point(6, 661);
            this.ux_labelMessageDisplay.Name = "ux_labelMessageDisplay";
            this.ux_labelMessageDisplay.Size = new System.Drawing.Size(114, 16);
            this.ux_labelMessageDisplay.TabIndex = 12;
            this.ux_labelMessageDisplay.Text = "Message Display";
            // 
            // ux_buttonCalcDormancyEstimatesNow
            // 
            this.ux_buttonCalcDormancyEstimatesNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCalcDormancyEstimatesNow.Location = new System.Drawing.Point(763, 19);
            this.ux_buttonCalcDormancyEstimatesNow.Name = "ux_buttonCalcDormancyEstimatesNow";
            this.ux_buttonCalcDormancyEstimatesNow.Size = new System.Drawing.Size(215, 23);
            this.ux_buttonCalcDormancyEstimatesNow.TabIndex = 11;
            this.ux_buttonCalcDormancyEstimatesNow.Text = "Calc Dormancy Estimates Now";
            this.ux_buttonCalcDormancyEstimatesNow.UseVisualStyleBackColor = true;
            this.ux_buttonCalcDormancyEstimatesNow.Visible = false;
            this.ux_buttonCalcDormancyEstimatesNow.Click += new System.EventHandler(this.ux_buttonCalcDormancyEstimatesNow_Click);
            // 
            // ViabilityWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 689);
            this.Controls.Add(this.ux_labelMessageDisplay);
            this.Controls.Add(this.ux_groupboxViabilityRule);
            this.Controls.Add(this.ux_buttonPostFinalResults);
            this.Controls.Add(this.ux_groupboxCreateViabilityTest);
            this.Controls.Add(this.ux_buttonSaveIVDChanges);
            this.Controls.Add(this.ux_buttonCancelIVDChanges);
            this.Controls.Add(this.ux_groupboxGetViabilityTest);
            this.Controls.Add(this.ux_panelInventoryViabilityData);
            this.Controls.Add(this.ux_buttonMakeNewInventoryViabilityData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViabilityWizard";
            this.Text = "Viability Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViabilityWizard_FormClosing);
            this.Load += new System.EventHandler(this.ViabilityWizard_Load);
            this.ux_panelInventoryViabilityData.ResumeLayout(false);
            this.ux_groupboxViabilityData.ResumeLayout(false);
            this.ux_groupboxViabilityDate.ResumeLayout(false);
            this.ux_groupboxViabilityDate.PerformLayout();
            this.ux_groupboxGetViabilityTest.ResumeLayout(false);
            this.ux_groupboxGetViabilityTest.PerformLayout();
            this.ux_groupboxCreateViabilityTest.ResumeLayout(false);
            this.ux_groupboxCreateViabilityTest.PerformLayout();
            this.ux_groupboxViabilityRule.ResumeLayout(false);
            this.ux_groupboxViabilityRule.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ux_buttonMakeNewInventoryViabilityData;
        private System.Windows.Forms.Button ux_buttonGetInventoryViabilityTest;
        private System.Windows.Forms.TextBox ux_textboxInventoryViabilityID;
        private System.Windows.Forms.Panel ux_panelInventoryViabilityData;
        private System.Windows.Forms.Label ux_labelInventoryViabilityID;
        private System.Windows.Forms.GroupBox ux_groupboxGetViabilityTest;
        private System.Windows.Forms.Label ux_labelInventoryID;
        private System.Windows.Forms.TextBox ux_textboxInventoryID;
        private System.Windows.Forms.Button ux_buttonPrintLabels;
        private System.Windows.Forms.GroupBox ux_groupboxViabilityDate;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount8;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount7;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount6;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount5;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount4;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount3;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount2;
        private System.Windows.Forms.RadioButton ux_radiobuttonCount1;
        private System.Windows.Forms.GroupBox ux_groupboxViabilitySummary;
        private System.Windows.Forms.GroupBox ux_groupboxViabilityData;
        private System.Windows.Forms.Button ux_buttonCancelIVDChanges;
        private System.Windows.Forms.Button ux_buttonSaveIVDChanges;
        private System.Windows.Forms.Button ux_buttonChangeDate;
        private System.Windows.Forms.ComboBox ux_comboboxViabilityRule;
        private System.Windows.Forms.CheckBox ux_checkboxShowAllRules;
        private System.Windows.Forms.Label ux_labelViabilityRule;
        private System.Windows.Forms.TextBox ux_textboxTotalSeeds;
        private System.Windows.Forms.Label ux_labelTotalSeeds;
        private System.Windows.Forms.TextBox ux_textboxNumberOfReplicates;
        private System.Windows.Forms.Label ux_labelReplicates;
        private System.Windows.Forms.TextBox ux_textboxViabilityTestedDate;
        private System.Windows.Forms.Label ux_labelViabilityTestedDate;
        private System.Windows.Forms.TextBox ux_textboxPercentViable;
        private System.Windows.Forms.Label ux_labelPercentViable;
        private System.Windows.Forms.GroupBox ux_groupboxCreateViabilityTest;
        private System.Windows.Forms.TextBox ux_textboxCreateFromOrder;
        private System.Windows.Forms.Label ux_labelCreateFromOrder;
        private System.Windows.Forms.Button ux_buttonCreateFromOrder;
        private System.Windows.Forms.TextBox ux_textboxCreateFromInventory;
        private System.Windows.Forms.Label ux_labelCreateFromInventory;
        private System.Windows.Forms.Button ux_buttonCreateFromInventory;
        private System.Windows.Forms.Button ux_buttonSaveIVChanges;
        private System.Windows.Forms.Button ux_buttonCancelIVChanges;
        private System.Windows.Forms.Button ux_buttonPostFinalResults;
        private System.Windows.Forms.GroupBox ux_groupboxViabilityRule;
        private System.Windows.Forms.Button ux_buttonCreateViabilityDataRecords;
        private System.Windows.Forms.DateTimePicker ux_datetimepickerChangeViabilityDate;
        private System.Windows.Forms.Label ux_labelMessageDisplay;
        private System.Windows.Forms.TextBox ux_textboxLighting;
        private System.Windows.Forms.Label ux_labelLighting;
        private System.Windows.Forms.TextBox ux_textboxTemperature;
        private System.Windows.Forms.TextBox ux_textboxPrechill;
        private System.Windows.Forms.TextBox ux_textboxMoisture;
        private System.Windows.Forms.TextBox ux_textboxSubstrata;
        private System.Windows.Forms.Label ux_labelTemperature;
        private System.Windows.Forms.Label ux_labelPrechill;
        private System.Windows.Forms.Label ux_labelMoisture;
        private System.Windows.Forms.Label ux_labelSubstrata;
        private System.Windows.Forms.Label ux_labelRuleNotes;
        private System.Windows.Forms.TextBox ux_textboxRuleNotes;
        private System.Windows.Forms.Label ux_labelTaxonomyNotes;
        private System.Windows.Forms.TextBox ux_textboxTaxonomyNotes;
        private System.Windows.Forms.Label ux_labelViabilityNotes;
        private System.Windows.Forms.TextBox ux_textboxViabilityNotes;
        private System.Windows.Forms.TextBox ux_textboxTaxon;
        private System.Windows.Forms.Label ux_labelTaxon;
        private System.Windows.Forms.ComboBox ux_comboboxCrystalReports;
        private System.Windows.Forms.Button ux_buttonCalcDormancyEstimatesNow;
    }
}


﻿/*
Copyright (C) 2020 International Potato Centre (CIP)

This file is part of PrintWizard, developed by Carlos Velásquez.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

This Software and the work of the developers shall be expressly acknowledged in any modified or derivative product based on the Software.

This notice shall be included in all copies or substantial portions of the Software.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

International Potato Centre
Apartado 1558, Lima 12, Peru
cip@cgiar.org - www.cipotato.org
*/

using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace PrintWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class PrintWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        string _originalPKeys = string.Empty;
        string _tablePKeys = string.Empty;
        DataSet _changedRecords = new DataSet();

        BindingSource _entityBindingSource;
        DataTable _dtEntity;
        DataTable _dtLabelTemplate;
        DataTable _dtPrinter;

        readonly string _AppSettingsName = "PrinterWizard_LabelSettings";
        LabelSettings _labelSettings;

        List<string> _labelVariables = new List<string>();
        string _labelTemplate = string.Empty;
        // {0}:print density {1}:label width {2}:label height {3}:label index(0)
        string _labelaryUrl = "http://api.labelary.com/v1/printers/{0}/labels/{1}x{2}/0/";

        int _labelIndex = 0;
        int _startRecordIndex = 0;
        int _endRecordIndex = 0;

        public string FormName
        {
            get
            {
                return "Print Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "";
            }
        }

        public PrintWizard()
        {
            InitializeComponent();
        }

        public PrintWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;

            _entityBindingSource = new BindingSource();
            _entityBindingSource.CurrentChanged += new EventHandler(_mainBindingSource_CurrentChanged);

            foreach (string pkeyToken in pKeys.Split(';'))
            {
                switch (pkeyToken.Split('=')[0].Trim().ToUpper())
                {
                    case ":ACCESSIONID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":INVENTORYID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":ORDERREQUESTID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":COOPERATORID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":GEOGRAPHYID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":TAXONOMYGENUSID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                    case ":CROPID":
                        if (pkeyToken.Split('=').Count() > 1 && !string.IsNullOrWhiteSpace(pkeyToken.Split('=')[1]))
                            _tablePKeys = pkeyToken;
                        break;
                }
                if (!string.IsNullOrEmpty(_tablePKeys))
                    break;
            }
        }

        private void PrintWizard_Load(object sender, EventArgs e)
        {
            try
            {
                //Set defaults
                ux_comboBoxSizeUnit.SelectedIndex = 0;
                ux_comboBoxUnit.SelectedIndex = 0;

                ux_textBoxAppSettingsName.Text = _AppSettingsName;
                ux_textBoxVariableStartsWith.Text = @"##";
                ux_textBoxVariableEndsWith.Text = @"##";
                ux_textBoxTemplateFolderPath.Text = System.Windows.Forms.Application.StartupPath + "\\Reports";
                ux_textBoxTemplateExtension.Text = ".prn";

                //Get Label settings from Local
                var labelSettingsJSON = WizardAppSettings.GetAppSettingValue(_AppSettingsName);

                if (!string.IsNullOrEmpty(labelSettingsJSON))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    _labelSettings = js.Deserialize<LabelSettings>(labelSettingsJSON);
                    ux_textBoxVariableEndsWith.Text = _labelSettings.VariableEndsWith;
                    ux_textBoxVariableStartsWith.Text = _labelSettings.VariableStarsWith;
                    ux_textBoxTemplateFolderPath.Text = _labelSettings.TemplateFolderPath;
                    ux_textBoxTemplateExtension.Text = _labelSettings.TemplateExtension;
                }
                else
                {
                    _labelSettings = new LabelSettings
                    {
                        VariableEndsWith = ux_textBoxVariableEndsWith.Text.Trim(),
                        VariableStarsWith = ux_textBoxVariableStartsWith.Text.Trim(),
                        TemplateFolderPath = ux_textBoxTemplateFolderPath.Text.Trim(),
                        TemplateExtension = ux_textBoxTemplateExtension.Text.Trim()
                    };
                }

                //Get label templates
                _dtLabelTemplate = new DataTable();
                _dtLabelTemplate.Columns.Add(new DataColumn() { ColumnName = "template_name", DataType = typeof(string) });
                _dtLabelTemplate.Columns.Add(new DataColumn() { ColumnName = "dataviews", DataType = typeof(string) });
                _dtLabelTemplate.Columns.Add(new DataColumn() { ColumnName = "zpl", DataType = typeof(string) });

                LoadReportsMapping();

                //Get default printer
                PrinterSettings ps = new PrinterSettings();
                ux_textboxPrinter.Text = ps.PrinterName;
                LoadPrinterProperties(ps);

                //Load Template Print Density
                List<KeyValuePair<string, string>> PrintDensityList = new List<KeyValuePair<string, string>>();
                PrintDensityList.Add(new KeyValuePair<string, string>("152dpi", "6dpmm"));
                PrintDensityList.Add(new KeyValuePair<string, string>("203dpi", "8dpmm"));
                PrintDensityList.Add(new KeyValuePair<string, string>("300dpi", "12dpmm"));
                PrintDensityList.Add(new KeyValuePair<string, string>("600dpi", "24dpmm"));
                ux_comboBoxTemplatePrintDensity.DataSource = PrintDensityList;
                ux_comboBoxTemplatePrintDensity.ValueMember = "Value";
                ux_comboBoxTemplatePrintDensity.DisplayMember = "Key";
                ux_comboBoxTemplatePrintDensity.SelectedValue = "8dpmm";

                //Get all printers
                _dtPrinter = GetAllPrinters();

                if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
                ux_dataGridViewDataview.DataSource = _entityBindingSource;

#if !DEBUG
                ux_tabcontrolMain.Controls.Remove(DataviewPage);
#endif
            }
            catch (WebException ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        #region Toolbar

        void _mainBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            try
            {
                if (_entityBindingSource.List.Count > 0 && ux_dataGridViewDataview.Rows.Count > 0)
                {
                    DataRowView drvCurrent = (DataRowView)_entityBindingSource.Current;

                    // Update the dataview Id on the Navigator Bar...
                    string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
                    bindingNavigatorEntityId.Text = drvCurrent.Row[pkeyName].ToString();

                    // Replace variables in template
                    string label = ReplaceVariables(_labelTemplate, ux_dataGridViewDataview.Rows[_entityBindingSource.Position]);

                    ux_pictureBoxPreview.Image = null;
                    Image image = GetImage(drvCurrent.Row[pkeyName].ToString(), label);
                    float labelWidth = FromInch((float)ux_numericUpDownLabelWidth.Value);
                    float labelHeight = FromInch((float)ux_numericUpDownLabelHeight.Value);
                    ux_pictureBoxPreview.Image = new Bitmap(image, Convert.ToInt32(labelWidth) * 2, Convert.ToInt32(labelHeight) * 2);
                }
                else
                {
                    bindingNavigatorEntityId.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void bindingNavigatorPreviewButton_Click(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                _labelIndex = 0;
                _startRecordIndex = 0;
                _endRecordIndex = _entityBindingSource.List.Count - 1;

                // Validation
                if (string.IsNullOrEmpty(ux_textboxPrinter.Text.Trim()))
                    throw new Exception("Select printer");
                if (string.IsNullOrEmpty(ux_textboxLabelTemplate.Text.Trim()))
                    throw new Exception("Select label template");
                if (string.IsNullOrEmpty(ux_textboxDataview.Text.Trim()))
                    throw new Exception("Select dataview");

                if (_entityBindingSource.List.Count == 0)
                    throw new Exception("No data to print");
                if (_dtEntity.PrimaryKey == null || _dtEntity.PrimaryKey.Count() == 0)
                    throw new Exception("Primary key not found in dataview");

                // Download all rendered labels
                if (_dtEntity != null)
                {
                    string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
                    foreach (DataGridViewRow dgvr in ux_dataGridViewDataview.Rows)
                    {
                        if (ImageCache.GetCachedImage(dgvr.Cells[pkeyName].Value.ToString()) == null)
                        {
                            string label = ReplaceVariables(_labelTemplate, dgvr);
                            ImageCache.SaveImageInCache(dgvr.Cells[pkeyName].Value.ToString(), DownloadLabel(label));
                        }
                    }
                }

                PrintDocument pd = new PrintDocument();
                PrinterSettings ps = new PrinterSettings() { PrinterName = ux_textboxPrinter.Text.Trim() };
                pd.PrinterSettings = ps;

                if (ux_comboBoxPrintResolution.SelectedItem != null)
                    pd.DefaultPageSettings.PrinterResolution = (PrinterResolution)ux_comboBoxPrintResolution.SelectedValue;
                if (ux_comboBoxPaperSize.SelectedItem != null)
                    pd.DefaultPageSettings.PaperSize = (PaperSize)ux_comboBoxPaperSize.SelectedValue;

                float marginLeft = FromInch((float)ux_numericUpDownMarginLeft.Value);
                float marginTop = FromInch((float)ux_numericUpDownMarginTop.Value);
                pd.DefaultPageSettings.Margins = new Margins((int)marginLeft, 0, (int)marginTop, 0);

                pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);

                PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();

                // Check if printer is Zebra
                if (IsZebraPrinter()) // ZPL suppported
                {
                    // Replace default print button
                    ToolStripButton ux_buttonPrintZPL = new ToolStripButton();
                    ux_buttonPrintZPL.Name = "ux_buttonPrintZPL";
                    ux_buttonPrintZPL.Image = ((ToolStripButton)((ToolStrip)printPrvDlg.Controls[1]).Items[0]).Image;
                    ux_buttonPrintZPL.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    ux_buttonPrintZPL.Text = "Print";
                    ux_buttonPrintZPL.Click += new EventHandler(this.ux_buttonPrintZPL_Click);
                    ((ToolStrip)printPrvDlg.Controls[1]).Items.RemoveAt(0);
                    ((ToolStrip)printPrvDlg.Controls[1]).Items.Insert(0, ux_buttonPrintZPL);
                }
                else // ZPL not supported
                {
                }

                //Language
                printPrvDlg.Name = "PrintWizard_bindingNavigatorPreviewButtonMessage1";
                _sharedUtils.UpdateControls(printPrvDlg.Controls, printPrvDlg.Name);
                
                // preview the assigned document 
                printPrvDlg.Document = pd;
                printPrvDlg.ShowDialog();

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
            catch (Exception ex)
            {
                Cursor.Current = origCursor;
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void bindingNavigatorPrintButton_Click(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                int labelsPerRecord = (int)ux_numericUpDownLabelsPerRecord.Value;
                _labelIndex = _entityBindingSource.Position * labelsPerRecord;
                _startRecordIndex = _entityBindingSource.Position;
                _endRecordIndex = _entityBindingSource.Position;

                // Validation
                if (string.IsNullOrEmpty(ux_textboxPrinter.Text.Trim()))
                    throw new Exception("Select printer");
                if (string.IsNullOrEmpty(ux_textboxLabelTemplate.Text.Trim()))
                    throw new Exception("Select label template");
                if (string.IsNullOrEmpty(ux_textboxDataview.Text.Trim()))
                    throw new Exception("Select dataview");

                if (_entityBindingSource.List.Count == 0)
                    throw new Exception("No data to print");
                if (_dtEntity.PrimaryKey == null || _dtEntity.PrimaryKey.Count() == 0)
                    throw new Exception("Primary key not found in dataview");

                // Check if printer is Zebra
                if (IsZebraPrinter()) // ZPL suppported
                {
                    // Generate ZPL for current record
                    string zpl = GenerateZPL();
                    RawPrinterHelper.SendStringToPrinter(ux_textboxPrinter.Text.Trim(), zpl);

                    Cursor.Current = origCursor;
                    new GGMessageBox("Document was sent to the printer", "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
                }
                else // ZPL not supported
                {
                    // Download rendered label
                    DataRowView drvCurrent = (DataRowView)_entityBindingSource.Current;
                    string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
                    if (ImageCache.GetCachedImage(drvCurrent.Row[pkeyName].ToString()) == null)
                    {
                        // Replace variables in template
                        string label = ReplaceVariables(_labelTemplate, ux_dataGridViewDataview.SelectedRows[0]);
                        ImageCache.SaveImageInCache(drvCurrent.Row[pkeyName].ToString(), DownloadLabel(label));
                    }

                    PrintDocument pd = new PrintDocument();
                    PrinterSettings ps = new PrinterSettings() { PrinterName = ux_textboxPrinter.Text.Trim() };
                    pd.PrinterSettings = ps;

                    if (ux_comboBoxPrintResolution.SelectedItem != null)
                        pd.DefaultPageSettings.PrinterResolution = (PrinterResolution)ux_comboBoxPrintResolution.SelectedValue;
                    if (ux_comboBoxPaperSize.SelectedItem != null)
                        pd.DefaultPageSettings.PaperSize = (PaperSize)ux_comboBoxPaperSize.SelectedValue;

                    float marginLeft = FromInch((float)ux_numericUpDownMarginLeft.Value);
                    float marginTop = FromInch((float)ux_numericUpDownMarginTop.Value);
                    pd.DefaultPageSettings.Margins = new Margins((int)marginLeft, 0, (int)marginTop, 0);

                    pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                    //pd.Print();

                    PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();
                    
                    //Language
                    printPrvDlg.Name = "PrintWizard_bindingNavigatorPreviewButtonMessage1";
                    _sharedUtils.UpdateControls(printPrvDlg.Controls, printPrvDlg.Name);

                    printPrvDlg.Document = pd;
                    printPrvDlg.ShowDialog();
                }

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
            catch (Exception ex)
            {
                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void bindingNavigatorPrintAllButton_Click(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                _labelIndex = 0;
                _startRecordIndex = 0;
                _endRecordIndex = _entityBindingSource.List.Count - 1;

                // Validation
                if (string.IsNullOrEmpty(ux_textboxPrinter.Text.Trim()))
                    throw new Exception("Select printer");
                if (string.IsNullOrEmpty(ux_textboxLabelTemplate.Text.Trim()))
                    throw new Exception("Select label template");
                if (string.IsNullOrEmpty(ux_textboxDataview.Text.Trim()))
                    throw new Exception("Select dataview");

                if (_entityBindingSource.List.Count == 0)
                    throw new Exception("No data to print");
                if (_dtEntity.PrimaryKey == null || _dtEntity.PrimaryKey.Count() == 0)
                    throw new Exception("Primary key not found in dataview");

                // Check if printer is Zebra
                if (IsZebraPrinter()) // ZPL suppported
                {
                    string zpl = GenerateZPL();
                    RawPrinterHelper.SendStringToPrinter(ux_textboxPrinter.Text.Trim(), zpl);

                    Cursor.Current = origCursor;
                    new GGMessageBox("Document was sent to the printer", "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
                }
                else // ZPL not supported
                {
                    // Download all rendered labels
                    if (_dtEntity != null)
                    {
                        string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
                        foreach (DataGridViewRow dgvr in ux_dataGridViewDataview.Rows)
                        {
                            if (ImageCache.GetCachedImage(dgvr.Cells[pkeyName].Value.ToString()) == null)
                            {
                                string label = ReplaceVariables(_labelTemplate, dgvr);
                                ImageCache.SaveImageInCache(dgvr.Cells[pkeyName].Value.ToString(), DownloadLabel(label));
                            }
                        }
                    }

                    PrintDocument pd = new PrintDocument();
                    PrinterSettings ps = new PrinterSettings() { PrinterName = ux_textboxPrinter.Text.Trim() };
                    pd.PrinterSettings = ps;

                    if (ux_comboBoxPrintResolution.SelectedItem != null)
                        pd.DefaultPageSettings.PrinterResolution = (PrinterResolution)ux_comboBoxPrintResolution.SelectedValue;
                    if (ux_comboBoxPaperSize.SelectedItem != null)
                        pd.DefaultPageSettings.PaperSize = (PaperSize)ux_comboBoxPaperSize.SelectedValue;

                    float marginLeft = FromInch((float)ux_numericUpDownMarginLeft.Value);
                    float marginTop = FromInch((float)ux_numericUpDownMarginTop.Value);
                    pd.DefaultPageSettings.Margins = new Margins((int)marginLeft, 0, (int)marginTop, 0);

                    pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                    pd.Print();
                }

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
            catch (Exception ex)
            {
                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private bool IsZebraPrinter()
        {
            // Get selected printer info
            var drPrinter = _dtPrinter.AsEnumerable().FirstOrDefault(x => x.Field<string>("Name").Equals(ux_textboxPrinter.Text.Trim()));
            if (drPrinter == null)
                throw new Exception("Printer not found");

            return drPrinter.Field<bool>("IsZebra");
        }

        private void ux_buttonPrintZPL_Click(object sender, EventArgs e)
        {
            try
            {
                string zpl = GenerateZPL();
                RawPrinterHelper.SendStringToPrinter(ux_textboxPrinter.Text.Trim(), zpl);
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private string GenerateZPL()
        {
            string zpl = string.Empty;

            int printerDPI = int.Parse(ux_comboBoxTemplatePrintDensity.Text.Replace("dpi", ""));
            float labelWidth = (float)ux_numericUpDownLabelWidth.Value * printerDPI;
            float labelHeight = (float)ux_numericUpDownLabelHeight.Value * printerDPI;

            float marginLeft = (float)ux_numericUpDownMarginLeft.Value * printerDPI;
            float marginTop = (float)ux_numericUpDownMarginTop.Value * printerDPI;
            float horizontalGap = (float)ux_numericUpDownHorizontalGap.Value * printerDPI;
            float verticalGap = (float)ux_numericUpDownVerticalGap.Value * printerDPI;

            int labelsPerRecord = (int)ux_numericUpDownLabelsPerRecord.Value;

            int columnsPerPage = (int)ux_numericUpDownHorizontalCount.Value;
            int rowsPerPage = 0;

            float paperHeight = ((PaperSize)ux_comboBoxPaperSize.SelectedValue).Height / 100.0F * printerDPI;

            if (labelHeight > paperHeight)
            {
                rowsPerPage = 1;
            }
            else if (2 * labelHeight + verticalGap > paperHeight) // 1 row
            {
                rowsPerPage = 1;
            }
            else //more than 1 row
            {
                rowsPerPage = 1 + (int)((paperHeight - labelHeight) / (labelHeight + verticalGap));
            }

            // Generate zpl
            string templateContent = _labelTemplate.Replace("^XA", "").Replace("^XZ", "");
            int zplX = 0, zplY = 0;
            int recordIndex = (_labelIndex / labelsPerRecord);

            while (recordIndex <= _endRecordIndex)
            {
                zpl += "^XA";
                for (int iRow = 0; iRow < rowsPerPage; iRow++)
                {
                    zplY = (int)(marginTop + iRow * (labelHeight + verticalGap));
                    for (int iCol = 0; iCol < columnsPerPage; iCol++)
                    {
                        zplX = (int)(marginLeft + iCol * (labelWidth + horizontalGap));
                        if (recordIndex <= _endRecordIndex)
                        {
                            zpl += $"^LH{zplX},{zplY}\n";
                            string label = ReplaceVariables(templateContent, ux_dataGridViewDataview.Rows[recordIndex]);
                            zpl += label + "\n";

                            _labelIndex++;
                            recordIndex = (_labelIndex / labelsPerRecord);
                        }
                        else
                            break;
                    }
                    if (recordIndex > _endRecordIndex)
                        break;
                }
                zpl += "^XZ";
            }

            return zpl;
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            if (_dtEntity == null || _dtEntity.Rows.Count == 0)
            {
                ev.HasMorePages = false;
                _labelIndex = 0;
                return;
            }
            Pen printPen = new Pen(Color.Black, 1);

            int rowsPerPage = 0;
            float yPos = 0;
            float xPos = 0;
            float horizontalGap = 0;
            float verticalGap = 0;
            int columnsPerPage;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            float labelWidth = FromInch((float)ux_numericUpDownLabelWidth.Value);
            float labelHeight = FromInch((float)ux_numericUpDownLabelHeight.Value);
            int labelsPerRecord = (int)ux_numericUpDownLabelsPerRecord.Value;

            var dipx = ev.Graphics.DpiX;
            var dipy = ev.Graphics.DpiY;
            var pageUnit = ev.Graphics.PageUnit;
            var marginBounds = ev.MarginBounds;

            columnsPerPage = (int)ux_numericUpDownHorizontalCount.Value;
            horizontalGap = FromInch((float)ux_numericUpDownHorizontalGap.Value);
            verticalGap = FromInch((float)ux_numericUpDownVerticalGap.Value);

            if (labelHeight > ev.MarginBounds.Height)
            {
                rowsPerPage = 1;
            }
            else if (2 * labelHeight + verticalGap > ev.MarginBounds.Height) // 1 row
            {
                rowsPerPage = 1;
            }
            else //more than 1 row
            {
                rowsPerPage = 1 + (int)((ev.MarginBounds.Height - labelHeight) / (labelHeight + verticalGap));
            }

            int recordIndex = (_labelIndex / labelsPerRecord);
            string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
            for (int iRow = 0; iRow < rowsPerPage; iRow++)
            {
                yPos = ev.MarginBounds.Top + iRow * (labelHeight + verticalGap);
                for (int iCol = 0; iCol < columnsPerPage; iCol++)
                {
                    xPos = ev.MarginBounds.Left + iCol * (labelWidth + horizontalGap);
                    if (recordIndex <= _endRecordIndex)
                    {
                        var pkeyEntity = ux_dataGridViewDataview.Rows[recordIndex].Cells[pkeyName].Value.ToString();
                        var image = ImageCache.GetCachedImage(pkeyEntity);
                        if (image != null)
                            ev.Graphics.DrawImage(image, xPos, yPos, labelWidth, labelHeight);
                        using (GraphicsPath path = RoundedRect(new Rectangle((int)xPos, (int)yPos, (int)labelWidth, (int)labelHeight), 5))
                        {
                            ev.Graphics.DrawPath(printPen, path);
                        }
                        _labelIndex++;
                        recordIndex = (_labelIndex / labelsPerRecord);
                    }
                    else
                        break;
                }
                if (recordIndex > _endRecordIndex)
                    break;
            }

            // If more labels exist, print another page.
            if (recordIndex <= _endRecordIndex)
            {
                ev.HasMorePages = true;
            }
            else
            {
                ev.HasMorePages = false;
                _labelIndex = _startRecordIndex * labelsPerRecord;
            }
        }

        #endregion

        #region PrintPage

        private void ux_textboxPrinter_Click(object sender, EventArgs e)
        {
            Cursor origCursor = Cursor.Current;
            try
            {
                GGWizard.LookupTablePicker3 ltp = new GGWizard.LookupTablePicker3(_sharedUtils, _dtPrinter, "", "Name", "Name") { IsListBoxStyled = true };
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    // Change cursor to the wait cursor...
                    Cursor.Current = Cursors.WaitCursor;
                    ux_textboxPrinter.Text = ltp.NewValue;

                    ux_comboBoxPaperSize.DataSource = null;
                    ux_comboBoxPrintResolution.DataSource = null;

                    var ps = new PrinterSettings { PrinterName = ltp.NewValue };
                    LoadPrinterProperties(ps);

                    Cursor.Current = origCursor;
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = origCursor;
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_textboxLabelTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                GGWizard.LookupTablePicker3 ltp = new GGWizard.LookupTablePicker3(_sharedUtils, _dtLabelTemplate, "", "template_name", "zpl") { IsListBoxStyled = true };
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    //Clean UI
                    ux_textboxDataview.Text = string.Empty;
                    ux_pictureBoxPreview.Image = null;
                    _entityBindingSource.DataSource = null;
                    //_dtEntity = null;

                    ux_textboxLabelTemplate.Text = ltp.NewValue;

                    //Remove invalid tags from template
                    _labelTemplate = CleanTemplate(ltp.NewKey);

                    //Get variables from template
                    LoadTemplateVariables();

                    //Get printer denstity and size from template name
                    Regex r = new Regex(@"(\d+)dpi_(\d+\.?\d*)x(\d+\.?\d*)in", RegexOptions.IgnoreCase);
                    Match m = r.Match(ux_textboxLabelTemplate.Text);
                    if (m.Success)
                    {
                        // density
                        ux_comboBoxTemplatePrintDensity.SelectedIndex = ux_comboBoxTemplatePrintDensity.FindStringExact(m.Groups[1].Value + "dpi");
                        // width
                        ux_numericUpDownLabelWidth.Value = decimal.Parse(m.Groups[2].Value);
                        // height
                        ux_numericUpDownLabelHeight.Value = decimal.Parse(m.Groups[3].Value);
                    }

                    //Select first dataview associated to template in LabelSettings
                    var dtTemplate = (DataRowView)ltp.SelectedItem;
                    var dataviews = dtTemplate.Row.IsNull("dataviews") ? "" : dtTemplate.Row.Field<string>("dataviews").Trim();
                    var dataview = dataviews.Split(new char[] { ',', ';' })[0].Trim();

                    if (!string.IsNullOrEmpty(dataview))
                    {
                        ux_textboxDataview.ReadOnly = true;
                        ux_textboxDataview.Text = dataview;

                        //Clear image cache;
                        ImageCache.Clear();

                        //Get Data from dateview and Bind to navigator toolstrip
                        GetAndBindDataview(dataview);
                    }
                    else
                    {
                        ux_textboxDataview.ReadOnly = false;
                    }
                }
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_textboxDataview_Click(object sender, EventArgs e)
        {
            try
            {
                if (ux_textboxDataview.ReadOnly)
                    return;

                DataTable dtSource = _sharedUtils.GetLocalData("select value_member, display_member from sys_dataview_lookup", "");
                GGWizard.LookupTablePicker3 ltp = new GGWizard.LookupTablePicker3(_sharedUtils, dtSource, "", "display_member", "value_member") { IsListBoxStyled = true };
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    //Clean UI
                    ux_pictureBoxPreview.Image = null;
                    _entityBindingSource.DataSource = null;
                    //_dtEntity = null;

                    ux_textboxDataview.Text = ltp.NewValue;

                    //Clear Image cache
                    ImageCache.Clear();

                    //Get Data from dateview and Bind to navigator toolstrip
                    GetAndBindDataview(ux_textboxDataview.Text);
                }
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_buttonRedraw_Click(object sender, EventArgs e)
        {
            try
            {
                if (_entityBindingSource.List.Count > 0)
                {
                    DataRowView drvCurrent = (DataRowView)_entityBindingSource.Current;
                    string label = _labelTemplate;

                    // Replace variables in template
                    label = ReplaceVariables(_labelTemplate, ux_dataGridViewDataview.SelectedRows[0]);

                    // Clear cache if label width, height or density has changed
                    ImageCache.Clear();

                    // Download image
                    Image image = DownloadLabel(label);

                    // Update image cache
                    string pkeyName = _dtEntity.PrimaryKey[0].ColumnName;
                    ImageCache.SaveImageInCache(drvCurrent.Row[pkeyName].ToString(), image);

                    // Update picturebox
                    float labelWidth = FromInch((float)ux_numericUpDownLabelWidth.Value);
                    float labelHeight = FromInch((float)ux_numericUpDownLabelHeight.Value);
                    ux_pictureBoxPreview.Image = new Bitmap(image, Convert.ToInt32(labelWidth) * 2, Convert.ToInt32(labelHeight) * 2);
                }
                else
                {
                    ux_pictureBoxPreview.Image = null;
                }
            }
            catch (Exception ex) // WebException
            {
                ux_pictureBoxPreview.Image = ux_pictureBoxPreview.ErrorImage;
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void LoadPrinterProperties(PrinterSettings ps)
        {
            PageSettings defaultPageSettings = ps.DefaultPageSettings;

            //Set default paper size
            List<KeyValuePair<string, PaperSize>> printerPaperSizeList = new List<KeyValuePair<string, PaperSize>>();
            PaperSize defaultPaperSize = defaultPageSettings.PaperSize;
            string defaultPaperSizeText = string.Format("{0} : {1} x {2}", defaultPaperSize.Kind, defaultPaperSize.Width / 100.0, defaultPaperSize.Height / 100.0);
            foreach (PaperSize paperSize in ps.PaperSizes)
            {
                var key = string.Format("{0} : {1} x {2}", paperSize.Kind, paperSize.Width / 100.0, paperSize.Height / 100.0);
                printerPaperSizeList.Add(new KeyValuePair<string, PaperSize>(key, paperSize));
            }
            ux_comboBoxPaperSize.DataSource = printerPaperSizeList;
            ux_comboBoxPaperSize.ValueMember = "Value";
            ux_comboBoxPaperSize.DisplayMember = "Key";
            ux_comboBoxPaperSize.SelectedIndex = ux_comboBoxPaperSize.FindStringExact(defaultPaperSizeText);

            //Set default printer resolution
            List<KeyValuePair<string, PrinterResolution>> printerResolutionList = new List<KeyValuePair<string, PrinterResolution>>();
            PrinterResolution defaultPrinterResolution = defaultPageSettings.PrinterResolution;
            string defaultPrinterResolutionText = string.Format(defaultPrinterResolution.Kind == PrinterResolutionKind.Custom ? "{0} : {1} x {2}" : "{0}",
                                                    defaultPrinterResolution.Kind, defaultPrinterResolution.X, defaultPrinterResolution.Y);
            foreach (PrinterResolution printerResolution in ps.PrinterResolutions)
            {
                var key = string.Format(printerResolution.Kind == PrinterResolutionKind.Custom ? "{0} : {1} x {2}" : "{0}", printerResolution.Kind, printerResolution.X, printerResolution.Y);
                printerResolutionList.Add(new KeyValuePair<string, PrinterResolution>(key, printerResolution));
            }
            ux_comboBoxPrintResolution.DataSource = printerResolutionList;
            ux_comboBoxPrintResolution.ValueMember = "Value";
            ux_comboBoxPrintResolution.DisplayMember = "Key";
            ux_comboBoxPrintResolution.SelectedIndex = ux_comboBoxPrintResolution.FindStringExact(defaultPrinterResolutionText);
        }

        private void GetAndBindDataview(string dataview)
        {
            DataSet ds = _sharedUtils.GetWebServiceData(dataview, _tablePKeys, 0, 0);
            if (ds != null && ds.Tables.Contains(dataview))
            {
                if (ds.Tables[dataview].Rows.Count == 0)
                    throw new Exception("Dataview is empty");
                if (ds.Tables[dataview].PrimaryKey == null || ds.Tables[dataview].PrimaryKey.Count() == 0)
                    throw new Exception("Primary key not found in dataview");

                _dtEntity = ds.Tables[dataview];
                _entityBindingSource.DataSource = _dtEntity;

                // Bind to datagridview
                if (ux_dataGridViewDataview.DataSource != _entityBindingSource)
                    ux_dataGridViewDataview.DataSource = _entityBindingSource;

                // Bind the bindingsource to the binding navigator toolstrip...
                if (ux_bindingnavigatorForm.BindingSource != _entityBindingSource)
                    ux_bindingnavigatorForm.BindingSource = _entityBindingSource;
            }
            else
            {
                _entityBindingSource.DataSource = null;
                throw new Exception("Error getting data from " + dataview + " dataview");
            }
        }

        private void LoadTemplateVariables()
        {
            _labelVariables.Clear();
            string startMark = string.IsNullOrEmpty(ux_textBoxVariableStartsWith.Text.Trim()) ? @"##" : ux_textBoxVariableStartsWith.Text.Trim();
            string endMark = string.IsNullOrEmpty(ux_textBoxVariableEndsWith.Text.Trim()) ? @"##" : ux_textBoxVariableEndsWith.Text.Trim();

            Regex regex = new Regex(startMark + "(.)*" + endMark);
            var matches = regex.Matches(_labelTemplate);
            foreach (var match in matches)
            {
                _labelVariables.Add(match.ToString()); //match.ToString().Replace(startMark,"").Replace(endMark,"");
            }
        }

        private string CleanTemplate(string template)
        {
            string cleanTemplate;
            int lastXA = template.LastIndexOf(@"^XA");
            int lastXZ = template.LastIndexOf(@"^XZ");
            if (lastXA < 0 || lastXZ < 0)
                throw new Exception("Bad zpl template\n^XA or ^XZ not found.");

            cleanTemplate = template.Substring(lastXA, lastXZ + 3 - lastXA);

            //Change International Font/Encoding
            cleanTemplate = cleanTemplate.Insert(3, @"^CI0");

            cleanTemplate = cleanTemplate.Replace(@"^MMT", "");
            //cleanTemplate = cleanTemplate.Replace(@"^FH\", "");
            cleanTemplate = Regex.Replace(cleanTemplate, @"\^PW[A-Za-z0-9,\\~]*", "");
            cleanTemplate = Regex.Replace(cleanTemplate, @"\^LL[A-Za-z0-9,\\~]*", "");
            cleanTemplate = Regex.Replace(cleanTemplate, @"\^LS[A-Za-z0-9,\\~]*", "");
            cleanTemplate = Regex.Replace(cleanTemplate, @"\^PQ[A-Za-z0-9,\\~]*", "");

            return cleanTemplate;
        }

        private DataTable GetAllPrinters()
        {
            DataTable dtPrinter = new DataTable();
            dtPrinter.Columns.Add("Name", typeof(string));
            dtPrinter.Columns.Add("Status", typeof(string));
            //dtPrinter.Columns.Add("Default", typeof(bool));
            dtPrinter.Columns.Add("Network", typeof(bool));
            dtPrinter.Columns.Add("Shared", typeof(bool));
            dtPrinter.Columns.Add("IsZebra", typeof(bool));

            using (ManagementObjectSearcher printerQuery = new ManagementObjectSearcher("SELECT Name,Status,Default,Network,Shared,DriverName from Win32_Printer"))
            using (ManagementObjectCollection printers = printerQuery.Get())
            {
                foreach (var printer in printers)
                {
                    var name = printer.GetPropertyValue("Name");
                    var status = printer.GetPropertyValue("Status");
                    //var isDefault = printer.GetPropertyValue("Default");
                    var isNetworkPrinter = printer.GetPropertyValue("Network");
                    var isShared = printer.GetPropertyValue("Shared");
                    var isZebra = printer.GetPropertyValue("DriverName").ToString().ToLowerInvariant().Contains("zdesigner ") ? true : false;
                    dtPrinter.Rows.Add(name, status, isNetworkPrinter, isShared, isZebra);
                }
            }
            return dtPrinter;
        }

        #endregion

        #region LabelSettingsPage

        private void ux_textBoxTemplateFolderPath_Click(object sender, EventArgs e)
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        ux_textBoxTemplateFolderPath.Text = fbd.SelectedPath;
                    }
                }
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_buttonNewTemplateMap_Click(object sender, EventArgs e)
        {
            try
            {
                if (_dtLabelTemplate != null)
                {
                    _dtLabelTemplate.Rows.Add("", "", "");
                    ux_dataGridViewLabelSettings.CurrentCell = ux_dataGridViewLabelSettings.Rows[ux_dataGridViewLabelSettings.Rows.Count - 1].Cells[0];
                }
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_buttonRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReportsMapping();
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_buttonSaveLabelSettings_Click(object sender, EventArgs e)
        {
            try
            {
                LabelSettings newSettings = new LabelSettings
                {
                    VariableEndsWith = ux_textBoxVariableEndsWith.Text.Trim(),
                    VariableStarsWith = ux_textBoxVariableStartsWith.Text.Trim(),
                    TemplateFolderPath = ux_textBoxTemplateFolderPath.Text.Trim(),
                    TemplateExtension = ux_textBoxTemplateExtension.Text.Trim()
                };
                if (!_labelSettings.Equals(newSettings))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    WizardAppSettings.SetAppSettingValue(_AppSettingsName, js.Serialize(newSettings));
                }

                DataTable dtTemplateChanges = _dtLabelTemplate.GetChanges();
                if (dtTemplateChanges != null)
                {
                    WizardAppSettings.SaveReportMapping(dtTemplateChanges);
                    _dtLabelTemplate.AcceptChanges();
                }

                new GGMessageBox("Settings saved successfully", "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void LoadReportsMapping()
        {
            ux_dataGridViewLabelSettings.DataSource = null;
            _dtLabelTemplate.Clear();

            if (string.IsNullOrEmpty(ux_textBoxTemplateExtension.Text.Trim()))
                ux_textBoxTemplateExtension.Text = ".prn";

            WizardAppSettings.GetReportMapping(_dtLabelTemplate, ux_textBoxTemplateExtension.Text.Trim());
            _dtLabelTemplate.AcceptChanges();

            if (string.IsNullOrEmpty(ux_textBoxTemplateFolderPath.Text.Trim()))
                ux_textBoxTemplateFolderPath.Text = System.Windows.Forms.Application.StartupPath + "\\Reports";

            foreach (string file in Directory.EnumerateFiles(ux_textBoxTemplateFolderPath.Text.Trim(), "*" + ux_textBoxTemplateExtension.Text.Trim()))
            {
                var dr = _dtLabelTemplate.AsEnumerable().FirstOrDefault(x => x.Field<string>("template_name").ToUpper().Equals(Path.GetFileName(file).ToUpper()));
                if (dr != null)
                {
                    dr["zpl"] = File.ReadAllText(file);
                    dr.AcceptChanges();
                }
                else
                {
                    var row = _dtLabelTemplate.NewRow();
                    row["template_name"] = Path.GetFileName(file);
                    row["dataviews"] = string.Empty;
                    row["zpl"] = File.ReadAllText(file);
                    _dtLabelTemplate.Rows.Add(row);
                }
            }
            ux_dataGridViewLabelSettings.DataSource = _dtLabelTemplate;

            ux_dataGridViewLabelSettings.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ux_dataGridViewLabelSettings.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void ux_dataGridViewLabelSettings_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataRow dr;

            if (!string.IsNullOrEmpty(columnName) && columnName.Equals("dataviews"))
            {
                DataTable dtSource = _sharedUtils.GetLocalData("select value_member, display_member from sys_dataview_lookup", "");
                GGWizard.LookupTablePicker3 ltp = new GGWizard.LookupTablePicker3(_sharedUtils, dtSource, "", "display_member", "value_member") { IsListBoxStyled = true };
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                    dr["dataviews"] = ltp.NewValue;
                }
                dgv.EndEdit();
            }
        }

        private void ux_dataGridViewLabelSettings_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (e.KeyCode == Keys.Delete)
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    // The user is deleting values from individual selected cells (not entire rows)...
                    foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                    {
                        if (dgvc.OwningColumn.Name.Equals("dataviews"))
                        {
                            dgvc.Value = "";
                            dgv.UpdateCellValue(dgvc.ColumnIndex, dgvc.RowIndex);
                        }
                    }
                }
                return;
            }
        }

        #endregion

        #region Utils

        public static float FromCm(float cmx)
        {
            const float cm2inch = 1 / 2.54F;
            return FromInch(cmx * cm2inch);
        }

        public static float FromInch(float inchx)
        {
            return inchx * 100;
        }

        public static System.Drawing.Drawing2D.GraphicsPath RoundedRect(Rectangle bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            Rectangle arc = new Rectangle(bounds.Location, size);
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();

            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }

            // top left arc  
            path.AddArc(arc, 180, 90);

            // top right arc  
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // bottom right arc  
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // bottom left arc 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        #endregion

        private Image DownloadLabel(string strZPL)
        {
            Image label = null;

            byte[] zpl = Encoding.UTF8.GetBytes(strZPL);
            // adjust print density, label width, label height, and label index
            var request = (HttpWebRequest)WebRequest.Create(string.Format(_labelaryUrl, ux_comboBoxTemplatePrintDensity.SelectedValue,
                                                                                ux_numericUpDownLabelWidth.Value.ToString(System.Globalization.CultureInfo.InvariantCulture),
                                                                                ux_numericUpDownLabelHeight.Value.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = zpl.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();

            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            {
                label = Bitmap.FromStream(stream);
            }
            return label;
        }

        private Image GetImage(string pkey, string label)
        {
            Image image = ImageCache.GetCachedImage(pkey); // Get from cache
            if (image == null) // Download if not exists in cache
            {
                image = DownloadLabel(label);
                if (image == null)
                    throw new Exception("Error downloading image from webservice");
                ImageCache.SaveImageInCache(pkey, image);
            }
            return image;
        }

        private string ReplaceVariables(string template, DataRow drCurrent)
        {
            string label = template;
            string startMark = string.IsNullOrEmpty(ux_textBoxVariableStartsWith.Text.Trim()) ? @"##" : ux_textBoxVariableStartsWith.Text.Trim();
            string endMark = string.IsNullOrEmpty(ux_textBoxVariableEndsWith.Text.Trim()) ? @"##" : ux_textBoxVariableEndsWith.Text.Trim();
            foreach (var variable in _labelVariables)
            {
                var variableName = variable.Replace(startMark, "").Replace(endMark, "");
                if (_dtEntity != null && _dtEntity.Columns.Contains(variableName))
                    label = label.Replace(variable, drCurrent[variableName].ToString());
            }
            return label;
        }

        private string ReplaceVariables(string template, DataGridViewRow dgrCurrent)
        {
            string label = template;
            string startMark = string.IsNullOrEmpty(ux_textBoxVariableStartsWith.Text.Trim()) ? @"##" : ux_textBoxVariableStartsWith.Text.Trim();
            string endMark = string.IsNullOrEmpty(ux_textBoxVariableEndsWith.Text.Trim()) ? @"##" : ux_textBoxVariableEndsWith.Text.Trim();
            foreach (var variable in _labelVariables)
            {
                var variableName = variable.Replace(startMark, "").Replace(endMark, "");
                if (ux_dataGridViewDataview.Columns.Contains(variableName))
                {
                    var formattedVlur = (dgrCurrent.Cells[variableName].FormattedValue == null || dgrCurrent.Cells[variableName].Value == DBNull.Value) ? string.Empty : dgrCurrent.Cells[variableName].FormattedValue.ToString();
                    label = label.Replace(variable, formattedVlur.Replace("~", @"\7E").Replace("^", @"\5E")
                        .Replace("á", @"\A0")
                        .Replace("é", @"\82")
                        .Replace("í", @"\A1")
                        .Replace("ó", @"\A2")
                        .Replace("ú", @"\A3")
                        .Replace("ñ", @"\A4")
                        .Replace("Ñ", @"\A5")
                        .Replace("ü", @"\81"));
                }
            }
            return label;
        }

        #region DGV control logic...

        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex < dv.Table.Columns.Count && //cv
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1
                && e.ColumnIndex < dv.Table.Columns.Count) //cv
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        #endregion

        private void bindingNavigatorAboutButton_Click(object sender, EventArgs e)
        {
            GGMessageBox ggMessageBox = new GGMessageBox("Print Wizard\n\n* * * * * * * * * * * * * * *\n"
                + @"Copyright (C) 2020 International Potato Centre (CIP)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.\n\nThis Software and the work of the developers shall be expressly acknowledged in any modified or derivative product based on the Software.

This notice shall be included in all copies or substantial portions of the Software.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

International Potato Centre
Apartado 1558, Lima 12, Peru
cip@cgiar.org - www.cipotato.org"
                + "\n* * * * * * * * * * * * * * *\n\nIcons by Icons8 [https://icons8.com]", "About", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
            DialogResult returnJunk = ggMessageBox.ShowDialog();
        }

        private void ux_textboxPrinter_TextChanged(object sender, EventArgs e)
        {

        }

        private void ux_buttonSaveAsUserSettings_Click(object sender, EventArgs e)
        {
            try
            {
                // Entries Validation
                if (string.IsNullOrEmpty(ux_textboxPrinter.Text.Trim()))
                    throw new Exception("Select printer");
                if (ux_comboBoxPaperSize.SelectedItem == null)
                    throw new Exception("Select paper size");
                if (string.IsNullOrEmpty(ux_textboxLabelTemplate.Text.Trim()))
                    throw new Exception("Select label template");
                if (string.IsNullOrEmpty(ux_textboxDataview.Text.Trim()))
                    throw new Exception("Select dataview");

                // Check if printer is Zebra
                if (IsZebraPrinter()) // ZPL suppported
                {
                    // Save as gg app user settings
                    var labelTemplate = new LabelTemplate {
                        TemplateName = ux_textboxLabelTemplate.Text.Trim(),
                        Dataview = ux_textboxDataview.Text.Trim(),
                        Density = int.Parse(ux_comboBoxTemplatePrintDensity.Text.Replace("dpi", "")),
                        Width = (float)ux_numericUpDownLabelWidth.Value,
                        Height = (float)ux_numericUpDownLabelHeight.Value,
                        MarginLeft = (float)ux_numericUpDownMarginLeft.Value,
                        MarginTop = (float)ux_numericUpDownMarginTop.Value,
                        HorizontalGap = (float)ux_numericUpDownHorizontalGap.Value,
                        VerticalGap = (float)ux_numericUpDownVerticalGap.Value,
                        PaperHeight = ((PaperSize)ux_comboBoxPaperSize.SelectedValue).Height / 100.0F,
                        PaperWidth = ((PaperSize)ux_comboBoxPaperSize.SelectedValue).Width / 100.0F,
                        HorizontalCount = (int)ux_numericUpDownHorizontalCount.Value,
                        Zpl = _labelTemplate
                    };

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    _sharedUtils.SaveUserSetting("PrintWizard", "LabelTemplateList", labelTemplate.TemplateName, js.Serialize(labelTemplate));
                    
                    //new GGMessageBox("Settings saved successfully", "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
                }
                else
                    throw new Exception("Only for zpl printers");
            }
            catch (Exception ex)
            {
                new GGMessageBox(ex.Message, "Print Wizard", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1).ShowDialog();
            }
        }

        private void ux_comboBoxUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(ux_comboBoxUnit.SelectedIndex > -1)
                {
                    switch (ux_comboBoxUnit.Text)
                    {
                        case "inches":
                            ux_numericUpDownHorizontalGap.Value = ux_numericUpDownHorizontalGap.Value / 25.4m;
                            ux_numericUpDownVerticalGap.Value = ux_numericUpDownVerticalGap.Value / 25.4m;
                            ux_numericUpDownMarginLeft.Value = ux_numericUpDownMarginLeft.Value / 25.4m;
                            ux_numericUpDownMarginTop.Value = ux_numericUpDownMarginTop.Value / 25.4m;

                            ux_numericUpDownPaperWidth.Value = ux_numericUpDownPaperWidth.Value / 25.4m;
                            ux_numericUpDownPaperHeight.Value = ux_numericUpDownPaperHeight.Value / 25.4m;
                            break;
                        case "mm":
                            ux_numericUpDownHorizontalGap.Value = ux_numericUpDownHorizontalGap.Value * 25.4m;
                            ux_numericUpDownVerticalGap.Value = ux_numericUpDownVerticalGap.Value * 25.4m;
                            ux_numericUpDownMarginLeft.Value = ux_numericUpDownMarginLeft.Value * 25.4m;
                            ux_numericUpDownMarginTop.Value = ux_numericUpDownMarginTop.Value * 25.4m;

                            ux_numericUpDownPaperWidth.Value = ux_numericUpDownPaperWidth.Value * 25.4m;
                            ux_numericUpDownPaperHeight.Value = ux_numericUpDownPaperHeight.Value * 25.4m;
                            break;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    class LabelSettings
    {
        public string VariableStarsWith { get; set; }
        public string VariableEndsWith { get; set; }
        public string TemplateFolderPath { get; set; }
        public string TemplateExtension { get; set; }

        public LabelSettings()
        {
            VariableStarsWith = string.Empty;
            VariableEndsWith = string.Empty;
            TemplateFolderPath = string.Empty;
            TemplateExtension = string.Empty;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            LabelSettings ls = (LabelSettings)obj;
            return VariableStarsWith.Equals(ls.VariableStarsWith) && VariableEndsWith.Equals(ls.VariableEndsWith)
                    && TemplateFolderPath.Equals(ls.TemplateFolderPath) && TemplateExtension.Equals(ls.TemplateExtension);
        }

        public override int GetHashCode()
        {
            return Tuple.Create(VariableStarsWith, VariableEndsWith, TemplateFolderPath, TemplateExtension).GetHashCode();
        }
    }

    static class ImageCache
    {
        private static readonly Dictionary<string, Image> _cacheEntries;
        private static readonly int _maxEntries;

        static ImageCache()
        {
            _cacheEntries = new Dictionary<string, Image>();
            _maxEntries = 1024;
        }

        /// <summary>
        /// Gets a cached version of an image
        /// specified by the key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Image GetCachedImage(string key)
        {
            Image img = null;

            if (_cacheEntries.ContainsKey(key))
                img = _cacheEntries[key];

            return img;
        }

        /// <summary>
        /// Saves an image in the image cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="img"></param>
        public static void SaveImageInCache(string key, Image img)
        {
            if (_cacheEntries.ContainsKey(key))
            {
                _cacheEntries[key] = img;
                return;
            }

            if (_cacheEntries.Count + 1 > _maxEntries)
            {
                _cacheEntries.Remove(_cacheEntries.Keys.First());
            }

            _cacheEntries.Add(key, img);
        }

        /// <summary>
        /// Clear all the image cache
        /// </summary>
        public static void Clear()
        {
            _cacheEntries.Clear();
        }
    }

    class LabelTemplate
    {
        public string TemplateName { get; set; }
        public float PaperWidth { get; set; }
        public float PaperHeight { get; set; }
        public float HorizontalGap { get; set; }
        public float VerticalGap { get; set; }
        public int HorizontalCount { get; set; }
        public float MarginLeft { get; set; }
        public float MarginTop { get; set; }

        public int Density { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public string Zpl { get; set; }
        public string Dataview { get; set; }
    }
}
